﻿using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;

namespace SB.IT.Integration.iConduct.Extensions
{
    public class AMSHandler
    {
        Logger logger;
        Files fileUtil;
        Dictionary<string, string> Parameters;
        DataTable Schema;

        private Dictionary<String, String> folderId2StatsContentMap = new Dictionary<string, string>();

        //params
        const string amsHostName = "amsHostName";
        const string amsUserName = "amsUserName";
        const string amsPassword = "amsPassword";
        const string amsLocalPath = "amsLocalPath";
        const string amsRemotePath = "amsRemotePath";
        const string amsFileName = "amsFileName";
        const string connectionString = "connectionString";
        const string tableName = "tableName";
        const string IconductBugWorkaround = "IconductBugWorkaround";

        //schema
        const string created = "ExecuteTime";
        const string jsonData = "jsonData";
        const string amsFolderId = "amsFolderId";

        public AMSHandler(Dictionary<string, string> parameters, DataTable schema)
        {
            this.Parameters = parameters;
            this.Schema = schema;
            logger = new Logger(null, null);
            fileUtil = new Files();
        }


        public void HandleAMSImport()
        {
            //download AMS folders and stats files from remote AMS server throgh ssh
            WinSCPHelper.sshDownloadRemotFolder(Parameters[amsHostName],Parameters[amsUserName],Parameters[amsPassword], Parameters[amsRemotePath],Parameters[amsLocalPath]);

            //find stats.log in folders
            string partialFileName = Parameters[amsFileName];// "stats.log";
            folderId2StatsContentMap = findFileInFolder(Parameters[amsLocalPath], partialFileName);

            //update schema with stat+folderId
            foreach (KeyValuePair<string, string> entry in folderId2StatsContentMap)
            {
                KWRequestHandler.UpdateSchema(entry.Key, entry.Value, this.Schema, "AMS");//, folderChangeDate
            }
            IconductBugWorkaroundFunc();
        }

        private void IconductBugWorkaroundFunc()
        {
            //------------------------------Workaround because IConduct bug - cannot set long value to schema column------------------------------
            if (this.Parameters.ContainsKey(IconductBugWorkaround) == true && Parameters[IconductBugWorkaround] == "true")
            {
                Dictionary<String, String> columnMaping = new Dictionary<string, string>() {  { jsonData, "json_data" }, { created, "created" }, { amsFolderId, amsFolderId } };
                using (var bulkCopy = new SqlBulkCopy(Parameters[connectionString], SqlBulkCopyOptions.KeepIdentity))
                {
                    // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                    //foreach (DataColumn col in this.Schema.Columns)
                    //{
                    //bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    bulkCopy.ColumnMappings.Add(amsFolderId, columnMaping[amsFolderId]);
                    //bulkCopy.ColumnMappings.Add(caseNumber, columnMaping[caseNumber]);
                    bulkCopy.ColumnMappings.Add(jsonData, columnMaping[jsonData]);
                    bulkCopy.ColumnMappings.Add(created, columnMaping[created]);
                    //}

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = Parameters[tableName];
                    bulkCopy.WriteToServer(this.Schema);
                }

                foreach (DataRow dr in this.Schema.Select($"{amsFolderId}<>''"))
                {
                    dr[jsonData] = "";
                }
            }
            //------------------------------Workaround because IConduct bug - cannot set long value to schema column------------------------------
        }

        private Dictionary<string, string> findFileInFolder(string path, string partialFileName)
        {
            Dictionary<String, String> folderName2FileContentMap = new Dictionary<string, string>();
            DirectoryInfo folderInWhichToSearch = new DirectoryInfo(path);
            FileInfo[] filesInDir = folderInWhichToSearch.GetFiles(partialFileName + "*", SearchOption.AllDirectories);
            //FileInfo[] filesInDir = Directory.GetFiles(path, "*" + partialFileName + "*", SearchOption.AllDirectories);

            foreach (FileInfo foundFile in filesInDir)
            {
                folderName2FileContentMap[foundFile.Directory.Name] = fileUtil.GetFileContent(foundFile.FullName);
            }

            return folderName2FileContentMap;
        }
    }
}
