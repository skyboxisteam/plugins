﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static SB.IT.Integration.iConduct.Extensions.Logger;

namespace SB.IT.Integration.iConduct.Extensions
{
    class KWObject
    {
        public DateTime maxSuccessDownloadFolderChangeDate { get; set; }
        public DateTime minFailureDownloadFolderChangeDate { get; set; }
        private DateTime lastSuccessDownloaDate;//save the last success file modified date for getting the lastSuccess  import in case of failure
        private bool errorInImportFile = false;

        public String folderId;
        public const String fileSeletedMessage = "{\n    \"message\": \"Entity is deleted\"\n}";

        Logger logger;
        public Dictionary<string, string> Parameters;
        public DataTable Schema;


        //params

        const string createTokenUrl = "createTokenUrl";


        const string deletFolderUrl = "deletFolderUrl";
        const string kwHostUrl = "kwHostUrl";
        const string contentType = "contentType";
        const string token = "token";
        const string clientAppId = "ClientAppId";
        const string userId = "UserId";
        const string sigKey = "SigKey";
        const string clientAppSecretKey = "ClientAppSecretKey";
        const string redirectURI = "RedirectURI";
        const string rootFolderID = "rootFolderID";
        const string currentFolderID = "currentFolderID";
        const string lastStatsImport = "lastStatsImport";
        const string apiVersion = "apiVersion";
        const string folderExpired = "folderExpired";
        const string maxUploadFileCount = "maxUploadFileCount";
        const string fileName = "fileName";
        const string connectionString = "connectionString";
        const string tableName = "tableName";
        const string configTableName = "configTableName";
        const string limitSearchRslt = "limitSearchRslt";
        const string debug = "debug";
        const string IconductBugWorkaround = "IconductBugWorkaround";


        
        //schema
        const string caseNumber = "caseNumber";
        const string created = "ExecuteTime";
        const string fileModified = "fileModified"; 
        const string jsonData = "jsonData";
        /*
        const string errorMessage = "errorMessage";
        const string statusCodeName = "StatusCodeName";
        const string statusDescription = "StatusDescription";
        */

        public KWObject(Dictionary<string, string> parameters, DataTable schema)
        {
            minFailureDownloadFolderChangeDate = DateTime.MaxValue;
            logger = new Logger(parameters, schema);
            this.Parameters = parameters;
            this.Schema = schema;
        }


        public CustomWebResponse DeleteFolder(String folderId = null)
        {
            if(folderId != null)
                this.folderId = folderId;
            CustomWebResponse cwr = null;
            try
            {
                if (String.IsNullOrEmpty(this.folderId))
                {
                    throw new Exception("folderId cannot be empty");
                }
                cwr = CustomWebRequest.Execute(Parameters[deletFolderUrl] + this.folderId,
                                        "DELETE",
                                        "",
                                        getStandardHeaders());
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), Logger.LogLevel.ERROR);
            }
            return cwr;
        }

        public CustomWebResponse CreateFolder(String parentFolderID, String folderName, String description)
        {

            CustomWebResponse cwr = null;
            try
            {
                String expireDate = Parameters[folderExpired] != null ? Parameters[folderExpired] : "2116-07-17";
                String reqBody = "{\"name\":\"" + folderName + "\",\"description\":\"" + description + "\",\"syncable\":false,\"expire\":\""+ expireDate + "\",\"fileLifetime\": 0,\"secure\":false}";
                cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/folders/{parentFolderID}/folders?returnEntity=true",
                                            "POST",
                                            "application/json",
                                            reqBody,
                                            getStandardHeaders(),
                                            getCustomHeaders());
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), Logger.LogLevel.ERROR);
            }
            return cwr;
        }

        public CustomWebResponse FindFolder(String folderName)
        {
           
            CustomWebResponse cwr = null;
            try
            {
                cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/folders/{Parameters[rootFolderID]}/folders?name:contains={folderName}",
                                            "GET",
                                            "application/json",
                                            "",
                                            getStandardHeaders(),
                                            getCustomHeaders());
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), Logger.LogLevel.ERROR);
            }
            return cwr;
        }

        internal void HandelFilesRetrivedBySearchAPI(dynamic data, string fileNamePattern)
        {
            if (data != null)
            {
                string fileId, fileName;
                //for each file: find the stasts file from zip file
                // get packlogs.zip from each data["files"] json
                for (int i = 0; i < data["files"].Count; i++)
                {
                    try
                    {
                        //update maxSuccessDownloadFolder paramenter if Download successfully or if no zip and no stats was found
                        string fileModifiedDate = data["files"][i]["modified"]; //ex 01/18/2019 18:47:52 //get the json format eith the Utc time zone (+2)
                        DateTime tfileModifiedDate = DateTime.ParseExact(fileModifiedDate, "G", System.Globalization.CultureInfo.InvariantCulture);
                        tfileModifiedDate = tfileModifiedDate.ToUniversalTime();//need to remove the utc time that was added

                        fileName = data["files"][i]["name"];
                        //if file name contain .zip
                        if (fileName.Contains(fileNamePattern))
                        {
                            fileId = data["files"][i]["id"];
                            string caseNumber = this.GetCaseNumberByFolderId(data["files"][i]["parentId"].ToString());//- folderId  which could represent "SF/00077000-00077999/00077446/Escalation"

                            DownloadFile(fileId, fileName, caseNumber, tfileModifiedDate);//maybe build list of object and call the function on the list
                        }
                        //save the last success file modified date for getting the lastSuccess  import in case of failure
                        //if no error occured
                        if (this.errorInImportFile == false) //this.lastSuccessDownloaDate != this.minFailureDownloadFolderChangeDate
                        {
                            this.lastSuccessDownloaDate = tfileModifiedDate;
                            if (tfileModifiedDate > this.maxSuccessDownloadFolderChangeDate)
                            {
                                this.maxSuccessDownloadFolderChangeDate = tfileModifiedDate;
                                if(this.maxSuccessDownloadFolderChangeDate < this.minFailureDownloadFolderChangeDate)//no previous error
                                    saveUnFailureFileDate(tfileModifiedDate);
                            }
                        }
                        else
                        {
                            //reset the error flag
                            this.errorInImportFile = false;
                            saveUnFailureFileDate(this.minFailureDownloadFolderChangeDate);//if error occured update the previous unFailure date
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.eventLog(ex.ToString(), LogLevel.ERROR);
                    }
                }
            }
            
        }

        public void saveUnFailureFileDate(DateTime tfileModifiedDate)
        {
            try
            {
                SqlConnection connection = new SqlConnection(Parameters[connectionString]);
                SqlCommand command = new SqlCommand($"SELECT * FROM {Parameters[configTableName]}", connection);
                command.Connection.Open();//query.ExecuteNonQuery(); needs iopen connection

                string querystr = $"UPDATE {Parameters[configTableName]} SET lastStatImport='{tfileModifiedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ")}'";
                SqlCommand query = new SqlCommand(querystr, connection);
                query.ExecuteNonQuery();

                command.Connection.Close();
            }
            catch (Exception ex)
            {
                logger.eventLog(ex.ToString(), LogLevel.ERROR);
            }
        }

        //return the case number represents the folderId, while returnning the third folder in the path hierarchy
        private string GetCaseNumberByFolderId(string folderId)
        {
            String caseNumber = "N/A";
                dynamic data = this.FindFolderById(folderId);
                // Get Case Folder FullPath
                string folderPath = data.path; //"SF/00077000-00077999/00077446/Escalation"
                string[] directories = folderPath.Split(new char[] { Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar }, options: StringSplitOptions.RemoveEmptyEntries);
                if (directories.Length > 2)
                    caseNumber = directories[2];
                else
                {
                    caseNumber = folderPath;
                }
                //returnning the son of the folder which contains the "-" char 
                //var caseNumberArr = folderPath.Split('/');
                //for (int i = caseNumberArr.Length - 1; i >= 0; i--)
                //{
                //    if (caseNumberArr[i].Contains("-"))//when looping from end to start we get to the case parent folder with the "-" pattern
                //    {
                //        break;
                //    }
                //    caseNumber = caseNumberArr[i];//00077446
                //}
            return caseNumber;

        }

        internal void HandelFilesRetrivedByActivitiesAPI(dynamic data, string fileNamePattern)
        {
            List<String> activityRelevantEvents = new List<string>() { "add_file_by_request", "add_file" };
            // If call success
            if (data != null)
            {

                string fileId, fileName, activityEvent;
                //for each file: find the file from file list in folder
                // get packlogs.zip from each data["data"][i] jason
                for (int i = 0; i < data["data"].Count; i++)
                {
                    try
                    {
                        //update maxSuccessDownloadFolder paramenter if Download successfully or if no zip and no stats was found
                        string tfolderChangeDate = data["data"][i]["created"]; //ex 10/01/2018 21:08:51
                        DateTime dfolderChangeDate = DateTime.ParseExact(tfolderChangeDate, "G", System.Globalization.CultureInfo.InvariantCulture);
                        if (dfolderChangeDate > this.maxSuccessDownloadFolderChangeDate)
                        {
                            this.maxSuccessDownloadFolderChangeDate = dfolderChangeDate;
                        }
                        //each data["data"][i] json result contain the folder's files, only one of them is packlog.zip
                        for (int j = 0; j < data["data"][i]["data"]["files"].Count; j++)
                        {

                            fileName = data["data"][i]["data"]["files"][j]["name"];
                            activityEvent = data["data"][i]["event"];
                            //if file name contain .zip and it's event of file uploaded by customer or sb user in KW system
                            if (fileName.Contains(fileNamePattern) && activityRelevantEvents.Contains(activityEvent))
                            {
                                fileId = data["data"][i]["data"]["files"][j]["fileId"];
                                //add k interval to iterate situation that case parent folder has sub folders - create rows for each folder
                                //(the irrelevant rows - rows that there folder name is not a valid case number- will be deleted in the IConduct)
                                //for (int k = 0; k < data["data"][i]["data"]["folders"].Count; k++)
                                //{
                                //string caseNumber = data["data"][i]["data"]["folders"][k]["name"];
                                string caseNumber = "";
                                string caseNumberPath = data["data"][i]["data"]["parentFolder"]["name"];//"SF/00077000-00077999/00077446/Escalation"
                                    var caseNumberArr = caseNumberPath.Split('/');
                                    for (int z = caseNumberArr.Length - 1; z >= 0; z--)
                                    {
                                        if (caseNumberArr[z].Contains("-"))//when looping from end to start we get to the case parent folder with the "-" pattern
                                        {
                                            break;
                                        }
                                        caseNumber = caseNumberArr[i];//00077446
                                    }
                                DownloadFile(fileId, fileName, caseNumber, dfolderChangeDate);//maybe build list of object and call the function on the list
                                //}
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.eventLog(ex.ToString(), LogLevel.ERROR);
                    }
                }
            }
        }


        public void DownloadFile(string fileId, string fileName, string caseNumber, DateTime folderChangeDate)
        {
            //debug values
            //fileId = "60068";
            //fileName = "server_packlogs_20180718_Natixis(kw_sf_apiuser@skyboxsecurity.com).zip";
            //caseNumber = "60062";

            //downloadFile
            logger.Log("KW Download File named :" + fileName + " Id: " + fileId);

            //download file from KW - WebClient is powerful tool to downloads files
            //string tmpFolder = @"C:\repository\";
            //string zipFileFullPath = tmpFolder + fileName;
            string targetFileName = "stats.log";
            string statContent = "";
            string zipFileFullPath = "";
            Stream strm = null;


            try
            {
                Dictionary<string, string> wcHeaders = new Dictionary<string, string>() { { "Authorization", $"Bearer {Parameters[token]}" }, { "Content-Type", $"application/zip" } };
                string filePath = Parameters[kwHostUrl] + $"/rest/files/{fileId}/content";
                Files fileUtil = new Files();
                zipFileFullPath = fileUtil.DownloadFile(filePath, fileName, wcHeaders);
                // If call success
                if (File.Exists(zipFileFullPath))
                {
                    strm = File.Open(zipFileFullPath, FileMode.Open, FileAccess.ReadWrite);
                    statContent = getFileContentFromZip(fileId, fileName, caseNumber, targetFileName, strm);
                    strm.Close();

                }
              
                //add chema row - with caseNumber, fileContent
                if (!String.IsNullOrWhiteSpace(statContent))
                {
                    //CaseNumber = "SF/00077000-00077999/00077446/Escalation"
                    DataTable tmpSchema = this.Schema.Clone();
                    //logger.Log("this.Schema :" + this.Schema.Rows.Count);
                    KWRequestHandler.UpdateSchema(caseNumber, statContent, tmpSchema, folderChangeDate);
                    InsertSchema(tmpSchema);
                    this.Schema.Rows.Add(tmpSchema.Rows);
                    //logger.Log("this.Schema after adding:" + this.Schema.Rows.Count);
                }
            }
            catch (WebException ex)
            {
                logger.Log(ex.ToString(), LogLevel.WARNING);
                // failed...
                if (ex.Response != null)
                {
                    using (StreamReader r = new StreamReader(
                        ex.Response.GetResponseStream()))
                    {
                        string responseContent = r.ReadToEnd();
                        logger.eventLog(ex.Message + ':' + responseContent, LogLevel.WARNING);
                        //if we find that the file cannot be download since entity deleted, we should not consider this as a failure
                        if (!responseContent.Equals(KWObject.fileSeletedMessage))
                        {
                            logFailureDate(folderChangeDate);
                        }
                    }
                }
                else
                {
                    logFailureDate(folderChangeDate);
                }
            }
            catch (Exception ex)
            {
                logger.Log(ex.ToString(), LogLevel.WARNING);
                //if we find that the file cannot be extracted because of unsupported zipping method, we should not consider this as a failure
                if (!ex.Message.Equals("Split or spanned archives are not supported."))
                {
                    logFailureDate(folderChangeDate);
                }
            }
            finally
            {
                //delete zip file
                try
                {
                    if (strm != null)
                    {
                        strm.Close();
                    }
                    File.Delete(zipFileFullPath);
                }
                catch (Exception ex)
                {
                    logger.Log("Error on delete: " + zipFileFullPath + ". " + ex.Message, LogLevel.WARNING);
                }
            }
        }

        private void logFailureDate(DateTime folderChangeDate)
        {
            //failed to download file - need to re download nextime 
            if (folderChangeDate < this.minFailureDownloadFolderChangeDate)
            {
                //this.minFailureDownloadFolderChangeDate = folderChangeDate;
                this.minFailureDownloadFolderChangeDate = this.lastSuccessDownloaDate;
                this.errorInImportFile = true;
            }
        }

        public void InsertSchema(DataTable Schema)
        {
            //------------------------------Workaround because IConduct bug - cannot set long value to schema column------------------------------
            if (this.Parameters.ContainsKey(IconductBugWorkaround) == true && Parameters[IconductBugWorkaround] == "true" && Schema.Rows.Count > 0)
            {
                Dictionary<String, String> columnMaping = new Dictionary<string, string>() { { caseNumber, caseNumber }, { jsonData, "json_data" }, { created, "created" }, { fileModified, fileModified } };
                using (var bulkCopy = new SqlBulkCopy(Parameters[connectionString], SqlBulkCopyOptions.KeepIdentity))
                {
                    // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                    //foreach (DataColumn col in this.Schema.Columns)
                    //{
                    //bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    bulkCopy.ColumnMappings.Add(caseNumber, columnMaping[caseNumber]);
                    bulkCopy.ColumnMappings.Add(jsonData, columnMaping[jsonData]);
                    bulkCopy.ColumnMappings.Add(created, columnMaping[created]);
                    bulkCopy.ColumnMappings.Add(fileModified, columnMaping[fileModified]);
                    //}

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = Parameters[tableName];
                    bulkCopy.WriteToServer(Schema);
                }

                foreach (DataRow dr in Schema.Select("caseNumber<>''"))
                {
                    dr[jsonData] = "";
                }
            }
            //------------------------------Workaround because IConduct bug - cannot set long value to schema column------------------------------
        }
        private string getFileContentFromZip(string fileId, string fileName, string caseNumber, string targetFileName, Stream strm)
        {
            string statContent="";
                //search for stas.log file in zip file - get targetFile contetct from inside zip file without unziping the zip folder
                try
                {
                //Find and Open stat.log file inside zip fle
                using (ZipArchive archive = new ZipArchive(strm, ZipArchiveMode.Update, true))//ZipFile.OpenRead(zipFileFullPath))
                {
                    using (StreamReader reader = new StreamReader(archive
                             .Entries.Where(x => x.Name.Equals(targetFileName,
                                                          StringComparison.InvariantCulture)) //.Union( GetNestedEntery(archive.Entries.Where(y => y.Name.Contains(".zip")).ToList(), zipFileFullPath))//to gandel cases where zip contain zip with stats
                             .FirstOrDefault()
                             .Open(), Encoding.UTF8))
                    {
                        //get json content from stat.log file
                        statContent = new string(
                                (reader
                                 .ReadToEnd())
                                 .ToArray());

                        reader.Close();
                    }
                    archive.Dispose();

                }
                    logger.Log("Found Stat file for case :" + caseNumber + " in file " + fileName);
                }
                catch (Exception ex)
                {
                    //did not find stats.log in the current zip
                    logger.Log("DownloadFile: Could not find " + targetFileName + " in " + fileName + " case :" + caseNumber + " file Id " + fileId + ". " + ex.Message, LogLevel.WARNING);
                    using (ZipArchive archive = new ZipArchive(strm, ZipArchiveMode.Update, true))
                    {
                        //look for nested zip
                        foreach (var zipEntry in archive.Entries.Where(x => x.Name.Contains(Parameters[KWObject.fileName])))
                        {
                            using (Stream entryStream = zipEntry.Open())
                            {
                                statContent = getFileContentFromZip(fileId, fileName, caseNumber, targetFileName, entryStream);
                                entryStream.Close();
                            }
                        }
                        archive.Dispose();
                    }
                }

            return statContent;
        }

        private IEnumerable<ZipArchiveEntry> GetNestedEntery(List<ZipArchiveEntry> list, string zipFileFullPath1)
        {
            using (ZipArchive archive = ZipFile.OpenRead(zipFileFullPath1 +"\\"+  list[0].Name))
            {
                throw new NotImplementedException();
            }
        }

        public JObject FindFolderById(String folderId)
        {

            CustomWebResponse cwr = null;
            try
            {
                cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/folders/{folderId}",
                                            "GET",
                                            "application/json",
                                            "",
                                            getStandardHeaders(),
                                            getCustomHeaders());
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), Logger.LogLevel.ERROR);
            }
            // If call success
            if (cwr.HeaderDictionary["StatusCode"].Equals("OK"))//200
            {
                dynamic data = JObject.Parse(cwr.Body);
                return data;
            }
            return null;
        }


        public CustomWebResponse FileRequest(String FolderID)
        {
            //logger.Log("FileRequest Start for folder: " + FolderID);
            CustomWebResponse cwr = null;
            try
            {
                String expireDate = Parameters[folderExpired] ?? "2116-07-17";
                String count = Parameters[maxUploadFileCount] ?? "20";
                String reqBody = "{\"to\":[\"" + Parameters[userId] + "\"],\"subject\":\"UploadFileRequest\",\"body\":\"UploadFileRequest\",\"expire\":\"" + expireDate + "\",\"count\": "+ count + ",\"requireAuth\":false,\"actionId\":\"1\",\"files\":[],\"secureBody\":false}";
                cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/folders/{FolderID}/actions/requestFile",
                                            "POST",
                                            "application/json",
                                            reqBody,
                                           getStandardHeaders(),
                                            getCustomHeaders());
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), Logger.LogLevel.ERROR);
            }
            return cwr;
        }

        // Get Case File Upload Link:
        public String GetKWFileRequestLink(String FolderID)
        {
            logger.Log("GetKWFileRequestLink Start for folder: " + FolderID);
            String RequestLink = "-1";
            CustomWebResponse cwr = FileRequest(FolderID);

            // If success
            if (cwr.HeaderDictionary["StatusCode"].Equals("OK"))//200
            {
                var data = JObject.Parse(cwr.Body);
                try
                {
                    //Extract value from {"variable": "UPLOAD_LINK","value": "https://kw-upload1.skyboxsecurity.com/w/snV1tsRB"}
                    RequestLink = data.SelectToken("$.data[0].variables[?(@.variable == 'UPLOAD_LINK')].value").Value<String>();
                }
                catch (Exception ex)
                {
                    logger.Log(ex.ToString(), Logger.LogLevel.ERROR);
                }
                
            }
            return RequestLink;
        }

        /// <summary>
        /// Retrive all zip files which were modified after lastExecuteDate and before lastExecuteDate + X minuts
        /// </summary>
        /// <param name="fileNamePattern"></param>
        /// <param name="lastExecuteDate"></param>
        /// <returns></returns>

        public JObject RetriveChangedFilesBySearchRestAPI(string fileNamePattern, string lastExecuteDate)
        {
           return RetriveChangedFilesBySearchRestAPI(fileNamePattern, 100, lastExecuteDate);
        }
        public JObject RetriveChangedFilesBySearchRestAPI(string fileNamePattern,int offset, string lastExecuteDate)
        {

            CustomWebResponse cwr = null;
            Parameters[limitSearchRslt] = Parameters.ContainsKey("limitSearchRslt") ? Parameters[limitSearchRslt] : "10000";
            try
            {
                //"searchType=f" to get only files; "&mode=full" returns details: modified and totalFilesCount; path:contains=zip to get only zip files
                cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/search?objectId={Parameters[rootFolderID]}&modified:gte={lastExecuteDate}&path:contains={fileNamePattern}" +
                                                                       $"&searchType=f&searchFilter=all&orderBy=modified:asc&limit={Parameters[limitSearchRslt]}&offset={offset}&mode=full",
                                               "GET",
                                               "application/json",
                                               "",
                                               getStandardHeaders(),
                                               getCustomHeaders());
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), Logger.LogLevel.ERROR);
            }

            // If call success
            if (cwr.HeaderDictionary["StatusCode"].Equals("OK"))//200
            {
                dynamic data = JObject.Parse(cwr.Body);
                return data;
            }
            return null;
        }
        public JObject RetriveChangedFilesByActivitiesRestAPI(String fileNamePattern, String lastExecuteDate)
        {

            CustomWebResponse cwr = null;
            Parameters[limitSearchRslt] = Parameters.ContainsKey("limitSearchRslt") ? Parameters[limitSearchRslt] : "10000";
            try
            {
                 cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/activities?topParentId={Parameters[rootFolderID]}&startDate={lastExecuteDate}&type=file_changes&orderBy=created:asc&limit={Parameters[limitSearchRslt]}",
                                                "GET",
                                                "application/json",
                                                "",
                                                getStandardHeaders(),
                                                getCustomHeaders());
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), Logger.LogLevel.ERROR);
            }

            // If call success
            if (cwr.HeaderDictionary["StatusCode"].Equals("OK"))//200
            {
                dynamic data = JObject.Parse(cwr.Body);
                return data;
            }
            return null;
        }

        private Dictionary<HttpRequestHeader, string> getStandardHeaders()
        {
            return new Dictionary<HttpRequestHeader, string>() { { HttpRequestHeader.Authorization, $"Bearer {Parameters[token]}" } };
        }

        private Dictionary<string, string> getCustomHeaders()
        {
            String tmpApiVersion = Parameters[apiVersion] != null ? Parameters[apiVersion] : "4.1";
            return new Dictionary<string, string>() { { "X-Accellion-Version", tmpApiVersion } };
        }
    }
}
