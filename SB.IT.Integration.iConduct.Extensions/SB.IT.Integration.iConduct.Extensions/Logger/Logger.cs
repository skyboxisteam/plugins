﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Extensions
{
    public class Logger : ILogger
    {
        [System.Flags]
        public enum LogLevel
        {
            TRACE,
            INFO,
            DEBUG,
            WARNING,
            ERROR,
            FATAL
        }

        //params
        const string debug = "debug";


        //Internal
        public Dictionary<string, string> Parameters;
        //public DataTable Schema;

        const string sSource = "iConduct Extension Plugin";
        const string sLog = "Application";

        public Logger(Dictionary<string, string> parameters, DataTable schema)
        {
            this.Parameters = parameters;
            //this.Schema = schema;
        }

        public void eventLog(String message, LogLevel logLevel, [CallerMemberName] string caller = null)
        {
            if (logLevel.Equals(LogLevel.DEBUG) && this.Parameters != null && (this.Parameters.ContainsKey(debug) == false || this.Parameters[debug] != "true") ) return;
            try
            {
                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);
                EventLog.WriteEntry(sSource, $"{logLevel.ToString()} in {caller} - {message}", logLevel == LogLevel.DEBUG ? EventLogEntryType.Information : logLevel == LogLevel.WARNING ? EventLogEntryType.Warning: EventLogEntryType.Error);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        public void Log(String logMessage, LogLevel logLevel, [CallerMemberName] string caller = null)
        {
            Console.WriteLine(logMessage);
            eventLog(logMessage, logLevel, caller);
        }
        public void Log(String logMessage, [CallerMemberName] string caller = null)
        {
            Console.WriteLine(logMessage);
            eventLog(logMessage, LogLevel.DEBUG, caller);
            //try
            //{
            //    using (StreamWriter w = File.AppendText("log.txt"))
            //    {
            //        Log(logMessage, w);
            //    }
            //}
            //catch (Exception e)
            //{
            //    if (!EventLog.SourceExists(sSource))
            //        EventLog.CreateEventSource(sSource, sLog);
            //    EventLog.WriteEntry(sSource, $"Error in {System.Reflection.MethodBase.GetCurrentMethod().Name} - {e.Message}", EventLogEntryType.Error);
            //}
        }
        //public static void Log(string logMessage, TextWriter w)
        //{
        //    w.Write("\r\nLog Entry : ");
        //    w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
        //        DateTime.Now.ToLongDateString());
        //    w.WriteLine("  :");
        //    w.WriteLine("  :{0}", logMessage);
        //    w.WriteLine("-------------------------------");
        //}
    }
}
