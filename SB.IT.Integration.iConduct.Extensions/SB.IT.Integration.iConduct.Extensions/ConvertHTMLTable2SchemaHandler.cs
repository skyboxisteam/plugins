﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SB.IT.Integration.iConduct.Extensions
{
    public class ConvertHTMLTable2SchemaHandler
    {
        public Dictionary<string, string> Parameters;
        public DataTable Schema;

        const string sSource = "iConduct Extension Plugin";
        const string sLog = "Application";
        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        ///  Constractor
        /// </summary>
        /// <param name="parameters">The parameters from Iconduct</param>
        /// <param name="schema"> The schema from Iconduct</param>

        public ConvertHTMLTable2SchemaHandler(Dictionary<string, string> parameters, DataTable schema)
        {
            this.Parameters = parameters;
            this.Schema = schema;
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Scanning the schema row by row and execute by row status
        /// </summary>
        /// <param name="schema"></param>
        /// <returns> The Schema after the rows has been scaned</returns>

        public DataTable HandleExtensionCall()
        {
            try
            {

                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(this.Parameters["html"]);
                DataTable dt = this.Schema;

                int i = 0;
                List<string> cols = new List<string>();
                foreach (HtmlAgilityPack.HtmlNode tr in doc.DocumentNode.SelectNodes("//tr"))
                {
                    int j = 0;
                    DataRow dr = dt.NewRow();
                    foreach (HtmlAgilityPack.HtmlNode td in tr.ChildNodes)
                    {
                        if (i == 0)
                        {
                            cols.Add(td.InnerText.Replace(' ', '_').Replace("(", "_").Replace(")", "_").Replace(":", "_").Replace("-", "_"));
                        }
                        else
                        {
                            string columnName = cols[j];
                            string value = td.InnerText;

                            if (dt.Columns.Contains(columnName))
                                dr[columnName] = value;
                        }
                        j++;
                    }
                    dt.Rows.Add(dr);
                    i++;
                }
            }
            catch (Exception e)
            {
                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);
                EventLog.WriteEntry(sSource, $"Error in {System.Reflection.MethodBase.GetCurrentMethod().Name} - {e.ToString()}", EventLogEntryType.Error);
            }
            return this.Schema;
        }
    }
}
