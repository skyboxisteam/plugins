﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Extensions
{
    static class CustomWebRequest
    {
        static Logger logger = new Logger(null, null);
        public static CustomWebResponse Execute(string url, string method, string requestBody, Dictionary<HttpRequestHeader, string> headerDictionary) 
        {
            return Execute( url, method, "application/json", requestBody, headerDictionary);
        }
        public static CustomWebResponse Execute(string url, string method, string contentType, string requestBody, Dictionary<HttpRequestHeader, string> headerDictionary)
        {
            return Execute(url, method, contentType, requestBody, headerDictionary, new Dictionary<string, string>());
        }
        public static CustomWebResponse Execute(string url, string method, string contentType, string requestBody, Dictionary<HttpRequestHeader, string> headerDictionary, Dictionary<string, string> customHeaders)
        {
            logger.Log("Start Execute, url: " + url + " requestBody: " + requestBody);

            Dictionary<string, string> header = new Dictionary<string, string>();
            string body = null;
            WebRequest request = WebRequest.Create(url);
            request.Method = method;
            request.ContentType = contentType;// "application/json";

            //logger.Log("Execute 1");
            foreach (KeyValuePair<HttpRequestHeader, string> entry in headerDictionary)
            {
                request.Headers.Add(entry.Key, entry.Value);
            }
            //logger.Log("Execute 2");
            foreach (KeyValuePair<string, string> entry in customHeaders)
            {
                request.Headers.Add(entry.Key, entry.Value);
            }
            //logger.Log("Execute 3");
            if (request.Headers.Count > 1)
            {
                Console.WriteLine(request.Headers.Get(1));
            }
            // request.Headers.Add(HttpRequestHeader.Authorization, basic);
            
            //logger.Log("Execute 4");
            if (!string.IsNullOrEmpty(requestBody))
            {
                //logger.Log("Execute Has requestBody: " + requestBody);
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                //logger.Log("Execute End requestBody handel");
            }
            try
            {
                //logger.Log("Execute 5");
                SSLValidator.OverrideValidation();
                //logger.Log("Execute After SSLValidator");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //logger.Log("Execute 6");
                for (int i = 0; i < response.Headers.Count; ++i)
                {
                    header.Add(response.Headers.Keys[i], response.Headers[i]);
                }

                //logger.Log("Execute 7");

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    body = reader.ReadToEnd();
                }

                //logger.Log("Execute 8");

                header.Add("StatusCode", response.StatusCode.ToString());
                header.Add("StatusDescription", response.StatusDescription.ToString());

                //logger.Log("Execute 9");
            }
            catch (WebException e)
            {
                header.Add("Error", e.Status.ToString());
                header.Add("ErrorMessage", e.Message);

                if (((HttpWebResponse)e.Response) != null)
                {
                    header.Add("StatusCode", ((HttpWebResponse)e.Response).StatusCode.ToString());
                    header.Add("StatusDescription", ((HttpWebResponse)e.Response).StatusDescription.ToString());
                    header.Add("Headers", ((HttpWebResponse)e.Response).Headers.ToString());
                }

                logger.Log("WebException: " + e.ToString(), Logger.LogLevel.ERROR);
            }
            catch (Exception e)
            {
                logger.Log("Exception: " + e.ToString(), Logger.LogLevel.ERROR);
            }

            //logger.Log("Execute 10");
            return new CustomWebResponse(header, body);
        }
    }


    public static class SSLValidator
    {
        private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain,
                                                  SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public static void OverrideValidation()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                OnValidateCertificate;
            ServicePointManager.Expect100Continue = true;
        }
    }

}

