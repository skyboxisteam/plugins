﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static SB.IT.Integration.iConduct.Extensions.Logger;

namespace SB.IT.Integration.iConduct.Extensions
{
    class Files
    {
        Logger logger = new Logger(null, null);
        /// <summary>
        /// Download cloud file from filePath, into local file targetFileName
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="targetFileName"></param>
        /// <param name="wcHeaders"></param>
        /// <returns></returns>
        public string DownloadFile(string filePath, string targetFileName = "myLog.txt", Dictionary<string, string> wcHeaders = null)
        {
            //string fileName = "myLog.txt";
            string tmpFolder = @"C:\repository\";
            string targetPath = tmpFolder + targetFileName;

            Utils.CreateWindowsFolder(tmpFolder);


            using (WebClient wc = new WebClient())
            {
                if (wcHeaders != null && wcHeaders.Count > 0)
                {
                    foreach (KeyValuePair<string, string> entry in wcHeaders)
                    {
                        wc.Headers[entry.Key] = entry.Value;
                    }
                }
                wc.DownloadFile(filePath, targetPath);
            }
            return targetPath;
        }
        /// <summary>
        /// fillter the file to contains only the relevant lines, which was created in the last X days apo.
        /// </summary>
        /// <param name="targetPath">local file path</param>
        /// <param name="daysAgo">number of days ago to define the relevant lines to return</param>
        public void RemoveLinesByDate(string targetPath, int daysAgo)
        {
            bool validDateCol = false;
            string line = null;
            string tmpFile = targetPath.Replace(".txt", "littelFile.txt");
            //string line_to_delete = "the line i want to delete";
            DateTime dt = DateTime.Now.AddDays(-daysAgo);//get only last 2 days
            using (StreamReader reader = new StreamReader(targetPath))
            {
                using (StreamWriter writer = new StreamWriter(tmpFile))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] columns = line.Split(
                                            new[] { "," },
                                            StringSplitOptions.None
                                            );
                        try
                        {
                            //get the date column - the first column
                            String lineDate = Convert.ToDateTime(columns[0], CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");//03/20/2015 16:48:33 => 2015-03-20
                            validDateCol = true;
                            if (String.Compare(lineDate, dt.ToString("yyyy-MM-dd")) <= 0)
                                continue;

                            writer.WriteLine(line);
                        }
                        catch (Exception e)
                        {
                            logger.eventLog(e.ToString(), LogLevel.ERROR);
                            continue;
                        }
                    }
                }
            }
            //if validDateCol is true - meaning fillter by date worked - because we found the right date column
            if (validDateCol)
            {
                try
                {
                    //Copy the filterd file into the origion file
                    File.Delete(targetPath);
                    File.Copy(tmpFile, targetPath);
                    File.Delete(tmpFile);
                }
                catch (Exception ex)
                {
                    logger.Log("Error on delete: "  + ex.Message, LogLevel.WARNING);
                }
            }
        }
        /// <summary>
        /// decrypt the requested sourceFile by running "fileName" exe on the "arguments" command 
        /// </summary>
        /// <param name="arguments">command with arguments for execution</param>
        /// <param name="fileName">the appliction tha runs the command</param>
        /// <returns></returns>
        public void decryptFileByBatchFile(string arguments, string fileName = "cmd.exe")
        {
            //string targetFile = sourceFile.Replace(".txt", "DecryptedFile.txt");

            //run the cmd in hidden mode
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = fileName;
            startInfo.Arguments = arguments;
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            // *** Redirect the output ***
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            // Redirect standard input
            startInfo.RedirectStandardInput = true;
            process.StartInfo = startInfo;
            process.Start();

            //string error = process.StandardError.ReadToEnd();
            // Write a "Y" to the process's input
            process.StandardInput.WriteLine("Y");

            // Now that we've sent the confirmation "Y" wait for the process to exit
            logger.Log("Before decryptFile");
            process.WaitForExit();
            logger.Log("After decryptFile");
            process.Close();
            process.Dispose();

        }

        public string GetFileContent(string filePath)
        {
            string statContent = "";
            using (StreamReader reader = new StreamReader(filePath))
            {
                //get json content from stat.log file
                statContent = new string(
                        (reader
                         .ReadToEnd())
                         .ToArray());

                reader.Close();
            }

            return statContent;
        }
    }
}
