﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Extensions
{
    internal class Utils
    {
        static public void CreateWindowsFolder(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }


        internal static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)

            {

                sb.Append(hash[i].ToString("X2"));

            }

            return "0x" + sb.ToString().ToLower();

        }

        //------------------------------ Athenticate util functions for create token ------------------------------------------------
        /// <summary>
        /// Return Random Number Between 2 Integer Numbers
        /// </summary>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        public static int GetRandomNumber(int minimum, int maximum)
        {
            Random random = new Random();
            return random.Next() * (maximum - minimum) + minimum;
        }
        /// <summary>
        /// Encode input with key by HMAC_SHA1 Cryptography protocol
        /// </summary>
        /// <returns></returns>
        public static string EncodeHMAC_SHA1(string input, string key)
        {
            byte[] byteKey = System.Text.Encoding.ASCII.GetBytes(key);
            byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(input);
            using (var myhmacsha1 = new HMACSHA1(byteKey))
            {
                var hashArray = myhmacsha1.ComputeHash(byteArray);
                return hashArray.Aggregate("", (s, e) => s + String.Format("{0:x2}", e), s => s);
            }
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }


        //------------------------------End Athenticate util functions for create token --------------
    }
}
