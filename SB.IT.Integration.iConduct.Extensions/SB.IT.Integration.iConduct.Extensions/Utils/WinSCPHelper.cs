﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace SB.IT.Integration.iConduct.Extensions
{
    class WinSCPHelper
    {
        //String hostName = "skyc-01";
        //String userName = "skyboxview";
        //String password = "skyboxview";
        //String remotePath = "/tmp/rmsfiles/prod";// All AMS customer folders by number //"/home/user/opt/repository";// 
        //String localPath = @"C:\repository\AMS";

        public static int sshDownloadRemotFolder(String hostName = "skyc-01", String userName = "skyboxview", String password = "skyboxview", String remotePath = "/tmp/rmsfiles/prod", String localPath = @"C:\repository\AMS")
        {
            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Scp,//Sftp throw exception  "Connection has been unexpectedly closed. Server sent command exit status 127.\r\nCannot initialize SFTP protocol. Is the host running an SFTP server?"
                    HostName = hostName,// "example.com",
                    UserName = userName,
                    Password = password
                    ,SshHostKeyFingerprint = "ssh-rsa-ab-ec-0e-5b-bc-e7-53-61-61-dc-c1-6a-f5-81-92-6c"
                    //,GiveUpSecurityAndAcceptAnySshHostKey = true //Give up security and accept any SSH host key. To be used in exceptional situations only, when security is not required. When set, log files will include warning about insecure connection. To maintain security, use  SshHostKeyFingerprint
                };

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult =
                        session.GetFiles(remotePath, localPath, false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                    }
                }

                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                return 1;
            }
        }
    }
}
