﻿using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Extensions
{
    public class TestHandler
    {
        Logger logger;
        KWObject kWObject;
        Dictionary<string, string> Parameters;
        DataTable Schema;

        private Dictionary<String, String> folderName2IdMap = new Dictionary<string, string>();

        //params
        const string kwHostUrl = "kwHostUrl";
        const string contentType = "contentType";
        const string token = "token";

        public TestHandler(Dictionary<string, string> parameters, DataTable schema)
        {
            this.Parameters = parameters;
            this.Schema = schema;
            logger = new Logger(null, null);
            kWObject = new KWObject(parameters, schema);
        }

        public void HandleExtensionCreate()
        {
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    CreateFolder($"Rina{i}");
                }
                catch (Exception e)
                {

                    logger.Log(e.ToString(), Logger.LogLevel.ERROR);
                }
            }
        }

        public void HandleExtensionDelete()
        {
            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    DeleteFolder($"Rina{i}");
                    //Thread.Sleep(1000);
                }
                catch (Exception e)
                {

                    logger.Log(e.ToString(), Logger.LogLevel.ERROR);
                }
            }
        }

        private void DeleteFolder(string folderName)
        {
            folderName2IdMap.TryGetValue(folderName, out kWObject.folderId);
            kWObject.DeleteFolder();
        }

        private void CreateFolder(String folderName)
        {
            String RinaFolderID = "65992"; //My Folder in SF
            string kwHostUrl = "https://kw-upload1.skyboxsecurity.com";

            String reqBody = "{\"name\":\"" + folderName + "\",\"description\":\"Test Number " + folderName + "\",\"syncable\":false,\"expire\":\"2116-07-17\",\"fileLifetime\": 0,\"secure\":false}";
            CustomWebResponse cwr = CustomWebRequest.Execute(kwHostUrl + $"/rest/folders/{RinaFolderID}/folders?returnEntity=true",
                                        "POST",
                                        "application/json",
                                        reqBody,
                                       new Dictionary<HttpRequestHeader, string>() { { HttpRequestHeader.Authorization, $"Bearer {Parameters[token]}" } },
                                       new Dictionary<string, string>() { { "X-Accellion-Version", "4.1" } });

            dynamic data = JObject.Parse(cwr.Body);
            string folderId = data["id"];
            folderName2IdMap.Add(folderName, folderId);
        }
    }
}
