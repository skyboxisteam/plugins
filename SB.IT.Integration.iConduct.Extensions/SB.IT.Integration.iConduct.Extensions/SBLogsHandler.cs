﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using static SB.IT.Integration.iConduct.Extensions.Logger;

namespace SB.IT.Integration.iConduct.Extensions
{
    public class SBLogsHandler
    {
        //params

        internal const string filePath = "filePath";
        internal const string decryptBatchFile = "decryptBatchFile";
        internal const string daysAgo = "daysAgo";


        //Dictionary & Updates Log schema
        internal const string externalId = "externalId";
        internal const string date = "date";
        internal const string customer = "customer";
        internal const string ip = "ip";
        internal Dictionary<int, string> mapColumnToIndex;

        //Internal

        protected Dictionary<string, string> Parameters;
        protected DataTable Schema;
        const string error = "Error";
        const string running = "running";

        protected Logger logger;
        Files fileUtil;
        const string batchDecrypyArgs = "/C {0} -demangle -logfile {1} {2}";


        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        ///  Constractor
        /// </summary>
        /// <param name="parameters">The parameters from Iconduct</param>
        /// <param name="schema"> The schema from Iconduct</param>

        public SBLogsHandler(Dictionary<string, string> parameters, DataTable schema)
        {
            this.Parameters = parameters;
            this.Schema = schema;
            logger = new Logger(parameters, schema);
            fileUtil = new Files();
            
        }

        public Dictionary<string, string> HandleExtension()
        {
            try
            {
                string myFilePath = Parameters[filePath];// "https://s3-us-west-2.amazonaws.com/skybox-update-logs/online_log.txt"; //https://s3-us-west-2.amazonaws.com/skybox-dictionary-logs/sb_request.log
                DownloadFile(myFilePath);
            }
            catch (Exception e)
            {
                logger.eventLog(e.Message, LogLevel.ERROR);
            }
            return Parameters;
        }
        /// <summary>
        /// this method dowloads the file from the reqeusted link, and add to schma the last relevant rows.
        /// it delete the file at the end of the proccess.
        /// </summary>
        /// <param name="filePath">link to csv file on cloud</param>
        protected virtual void DownloadFile(string filePath)
        {
            string targetPath = fileUtil.DownloadFile(filePath, "UpdatesLog.txt");

            // If DownloadFile call success
            if (File.Exists(targetPath))
            {
                //int daysAgo = 2;
                fileUtil.RemoveLinesByDate(targetPath,int.Parse( this.Parameters[daysAgo]));//Delete from file all the lines older then X last days
                
                string line;
                // Read the file and display it line by line.  
                StreamReader file = new StreamReader(targetPath);
                while ((line = file.ReadLine()) != null)
                {
                    AddRowToSchema(line);
                }

                file.Close();
                try
                {
                    File.Delete(targetPath);
                }
                catch (Exception ex)
                {
                    logger.Log("Error on delete: " + targetPath + ". " + ex.Message, LogLevel.WARNING);
                }
            }
        }
        /// <summary>
        /// Decrypt the source file, the output is a decrypted file. 
        /// </summary>
        /// <param name="sourceFile">local file to decrypt</param>
        /// <param name="batchFile">local .bat file</param>
        /// <returns>the decrypted file path</returns>
        protected string decryptFile(string sourceFile ,string batchFile)
        {
            string targetFile = sourceFile.Replace(".txt", "DecryptedFile.txt");

            string exeName = "cmd.exe";
            string arguments = String.Format(batchDecrypyArgs, batchFile, sourceFile, targetFile);// $"/C {batchFile} -demangle -logfile {sourceFile} {targetFile}";
            fileUtil.decryptFileByBatchFile(arguments, exeName);

                       
            return targetFile;
        }

        /// <summary>
        /// add each line, which is seperated by commas, to the proper schema column by the map
        /// create hashed externalId
        /// </summary>
        /// <param name="line"></param>
        protected void AddRowToSchema(string line)
        {
            const string ignoreValue = ", ";//remove comma character from customer name.
            const string newValue = "##";
            if (!String.IsNullOrWhiteSpace(line))
                {
                    DataRow row = this.Schema.NewRow();
                    if (line.Contains(ignoreValue))
                    {
                        line = line.Replace(ignoreValue, newValue);
                    }
                    string[] columns = line.Split(
                                           new[] { ","},
                                           StringSplitOptions.None
                                           );
                //logger.Log(line);
                //logger.Log(columns.Length.ToString());
                    for (int i = 0; i < columns.Length; i++)
                    {
                        if (mapColumnToIndex.Count <= i)
                        {
                            logger.Log("The follwing row is invalid: "+line,LogLevel.ERROR);
                            return;
                        }
                        row[mapColumnToIndex[i]] = columns[i].Replace(newValue, ignoreValue);//return the comma special char to the customer name
                    }
                    row[externalId] = Utils.CalculateMD5Hash(row[customer].ToString() + row[date]);
                    this.Schema.Rows.Add(row);
                }
        }
    }
}