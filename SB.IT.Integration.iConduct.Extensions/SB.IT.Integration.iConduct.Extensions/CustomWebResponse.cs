﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Extensions
{
    class CustomWebResponse
    {
        public Dictionary<string, string> HeaderDictionary { get; set; }
        public string Body { get; set; }


        public CustomWebResponse(Dictionary<string, string> header, string body)
        {
            HeaderDictionary = header;
            Body = body;
        }
    }
}
