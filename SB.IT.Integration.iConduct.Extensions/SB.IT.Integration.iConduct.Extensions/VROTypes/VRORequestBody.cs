﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
    class VRORequestBody
    {
        const string vmName = "vmName";
        const string vmToCloneName = "vmToCloneName";
        const string vmToDeleteName = "vmToDeleteName";
        public VROParameter[] parameters { get; set; }
   
        public VRORequestBody(string vmOriginalName)
        {
            parameters = new VROParameter[1];
            this.parameters[0] = new VROParameter("vmToDeleteName", vmOriginalName);       
        }

        public VRORequestBody(string vmOriginalName, string vmNewdName)
        {
          parameters = new VROParameter[2];
          this.parameters[0] = new VROParameter("vmName", vmNewdName);
          this.parameters[1] = new VROParameter("vmToCloneName", vmOriginalName);
        }

        public VRORequestBody(string vmOriginalName, int power, string param1Type  )
        {
            parameters = new VROParameter[2];
            this.parameters[0] = new VROParameter("vmName", vmOriginalName);
            this.parameters[1] = new VROParameter("power", power, param1Type, "local");
        }
    }
}
