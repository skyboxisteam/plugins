﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
 
    class VROValueBooolean
    {
        [JsonProperty("boolean")]
        public VROString Boolean { get; set; }
         public VROValueBooolean(VROString s)
        {
            this.Boolean = s;
        }

}
}
