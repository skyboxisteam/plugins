﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
    class VRORelations
    {

        [JsonProperty("link")]
        public VROLink[] Link { get; set; }
    }
}
