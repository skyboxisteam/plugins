﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
 
    class VROValue
    {
        [JsonProperty("string")]
        public VROString String { get; set; }
         public VROValue(VROString s)
        {
            this.String = s;
        }

}
}
