﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
    class VROStatusResponse
    {
        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("relations")]
        public VRORelations Relations { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("start-date")]
        public long StartDate { get; set; }

        [JsonProperty("end-date")]
        public long EndDate { get; set; }

        [JsonProperty("business-state")]
        public string BusinessState { get; set; }

        [JsonProperty("started-by")]
        public string StartedBy { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("content-exception")]
        public string ContentException { get; set; }

        [JsonProperty("current-item-display-name")]
        public string CurrentItemDisplayName { get; set; }

        [JsonProperty("input-parameters")]
        public VROParameter[] InputParameters { get; set; }

        [JsonProperty("output-parameters")]
        public VROParameter[] OutputParameters { get; set; }
    }
}
