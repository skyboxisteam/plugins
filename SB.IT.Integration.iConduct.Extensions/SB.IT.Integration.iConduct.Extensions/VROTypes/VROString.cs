﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
    class VROString
    {
        [JsonProperty("value")]
        public object Value { get; set; }

        public VROString(object valueParam)
        {
            this.Value = valueParam;
        }
        
    }
}
