﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
    class VROInputParam
    {
        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public VROValue Value { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }
    }
}
