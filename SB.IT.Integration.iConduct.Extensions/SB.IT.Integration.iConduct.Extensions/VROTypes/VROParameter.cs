﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.IT.Integration.iConduct.Extensions.VROTypes
{
    class VROParameter
    {
        public string name { get; set; }
        public string scope { get; set; }
        public string type { get; set; }
        public object value { get; set; }

        public VROParameter()
        {

        }
        public VROParameter(string nameParam, string valueParam)
        {
            this.name = nameParam;
            this.scope = "local";
            this.type = "string";
            this.value = new VROValue(new VROString(valueParam));

        }

        public VROParameter(string nameParam, int valueParam, string type, string scope)
        {
            this.name = nameParam;
            this.scope = scope;
            this.type = type;
            this.value = new VROValueBooolean(new VROString(valueParam));

        }
    }
}
