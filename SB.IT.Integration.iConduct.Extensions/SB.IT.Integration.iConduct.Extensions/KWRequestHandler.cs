﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.Net;
using System.Security.Cryptography;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Data.SqlClient;
using static SB.IT.Integration.iConduct.Extensions.Logger;
using System.Globalization;

namespace SB.IT.Integration.iConduct.Extensions
{

    public class KWRequestHandler
    {
        //params

        const string createTokenUrl = "createTokenUrl";


        const string deletFolderUrl = "deletFolderUrl";
        const string kwHostUrl = "kwHostUrl";
        const string contentType = "contentType";
        const string token = "token";
        const string clientAppId = "ClientAppId";
        const string userId = "UserId";
        const string sigKey = "SigKey";
        const string clientAppSecretKey = "ClientAppSecretKey";
        const string redirectURI = "RedirectURI";
        const string rootFolderID = "rootFolderID";
        const string currentFolderID = "currentFolderID";
        const string lastStatsImport = "lastStatsImport";
        const string fileName = "fileName";
        const string connectionString = "connectionString";
        const string tableName = "tableName";
        const string configTableName = "configTableName";
        const string limitSearchRslt = "limitSearchRslt";
        const string debug = "debug";
        const string IconductBugWorkaround = "IconductBugWorkaround";


        //schema

        const string tokenSchemaCol = "Token";
        const string folderId = "KW_FolderID__c";
        const string upldFileLink = "KW_CaseFolderUploadLink__c";
        const string folderLink = "KW_Folder_Link2__c";
        const string caseNumber = "caseNumber";
        const string created = "ExecuteTime";
        const string fileModified = "fileModified"; 
        const string currentFolderIDSchemaCol = "CurrentFolderID";
        const string folderChangeDate = "folderChangeDate";
        const string jsonData = "jsonData";
        const string amsFolderId = "amsFolderId";

        const string errorMessage = "errorMessage";
        const string statusCodeName = "StatusCodeName";
        const string statusDescription = "StatusDescription";

        //Internal

        public Dictionary<string, string> Parameters;
        public DataTable Schema;
        const string error = "Error";
        const string running = "running";

        const string sSource = "iConduct Extension Plugin";
        const string sLog = "Application";

        KWObject kWObject;
        Logger logger;
        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        ///  Constractor
        /// </summary>
        /// <param name="parameters">The parameters from Iconduct</param>
        /// <param name="schema"> The schema from Iconduct</param>

        public KWRequestHandler(Dictionary<string, string> parameters, DataTable schema)
        {
            this.Parameters = parameters;
            this.Schema = schema;

            logger = new Logger(parameters, schema);
            kWObject = new KWObject(parameters, schema);
        }


        public DataTable ImportFilesPluginHandleExtension()
        {
            try
            {
                //String tcurrentFolderID = Parameters[currentFolderID];//get parent currentFolderID from CS
                //String tlastStatsImport = Parameters[lastStatsImport];//"2018-08-30";//
                DateTime tlastStatsImport = getLastImportStat();
                String tfileName = Parameters[fileName];//".zip";//
                ImportFiles(tfileName, tlastStatsImport);
            }
            catch (Exception e)
            {
                logger.eventLog(e.ToString(), LogLevel.ERROR);
            }

            return this.Schema;
        }

        private void ImportFiles(string fileNamePattern, String lastExecuteDate)
        {
            ImportFiles(fileNamePattern, DateTime.Parse(lastExecuteDate, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal)); //DateTime.Parse(lastExecuteDate);)
        }
            private void ImportFiles(string fileNamePattern, DateTime lastExecuteDate)
        {
            kWObject.minFailureDownloadFolderChangeDate = DateTime.MaxValue;
            kWObject.maxSuccessDownloadFolderChangeDate = lastExecuteDate;// DateTime.Parse(lastExecuteDate, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal); //DateTime.Parse(lastExecuteDate);
            
            logger.Log("KW Import Files contains :" + fileNamePattern + " that was creared after: "+ lastExecuteDate);
            logger.Log("KW kWObject.maxSuccessDownloadFolderChangeDate :" + kWObject.maxSuccessDownloadFolderChangeDate);
 
            int totalFilesCount = 0;
            int offset = 0;//offset to retrive the next bulk
            string searchDate = kWObject.maxSuccessDownloadFolderChangeDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");//need tmp var because kWObject.maxSuccessDownloadFolderChangeDate is changing in kWObject
            //keep query KW until offset is more than the total files retuned as a result of the search
            do
            {
                dynamic data = kWObject.RetriveChangedFilesBySearchRestAPI(fileNamePattern,offset, searchDate);
                //dynamic data = kWObject.RetriveChangedFilesByActivitiesRestAPI(fileNamePattern, kWObject.maxSuccessDownloadFolderChangeDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"));
                if (data != null)
                {
                    //get search api response fies total count (regardless the offset)
                    totalFilesCount = data.metadata.totalFilesCount;
                    //if there any zip files in return from the search retrieve their stats files
                    if (totalFilesCount > 0)
                    {
                        kWObject.HandelFilesRetrivedBySearchAPI(data, fileNamePattern);
                        //kWObject.HandelFilesRetrivedByActivitiesAPI(data, fileNamePattern);
                    }
                }
                //add to offset the bulk we retrive in each iteration
                offset += String.IsNullOrWhiteSpace(Parameters[limitSearchRslt]) ? 100 : Int32.Parse( Parameters[limitSearchRslt]) ;
                //continue retrieving files if next retrived offset is smaller than the total existinf files
            } while (totalFilesCount > 0 && offset < totalFilesCount);

            //IconductBugWorkaroundFunc();
                    
            //updateSchemaOnLastSuccessfullImportStatsDate(); comment by Rina 28.3.19, the date is updated after each file download 
        }

        //Config is helper table that holds the lastStatImport date of the failure or the unfailure downloaded file 
        private DateTime getLastImportStat()
        {
            String configTable = Parameters[configTableName] ?? "Config";
            SqlConnection conn = new SqlConnection(Parameters[connectionString]);
            conn.Open();
                       
            SqlCommand command = new SqlCommand("Select lastStatImport from "+ configTable, conn);
            /*
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    // iterate your results here
                    Console.WriteLine(String.Format("{0}", reader["lastStatImport"]));
                    //get first column
                    DateTime rslt = reader.GetDateTime(0);

                }
            }
            */
            //get first column in first row
            DateTime result = (DateTime)(command.ExecuteScalar());
            return result;

        }
        public void IconductBugWorkaroundFunc(DataTable Schema)
        {
            //------------------------------Workaround because IConduct bug - cannot set long value to schema column------------------------------
            if (this.Parameters.ContainsKey(IconductBugWorkaround) == true && Parameters[IconductBugWorkaround] == "true" && this.Schema.Rows.Count > 0)
            {
                Dictionary<String, String> columnMaping = new Dictionary<string, string>() { { caseNumber, caseNumber }, { jsonData, "json_data" }, { created, "created" }, { fileModified, fileModified } };
                using (var bulkCopy = new SqlBulkCopy(Parameters[connectionString], SqlBulkCopyOptions.KeepIdentity))
                {
                    // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                    //foreach (DataColumn col in this.Schema.Columns)
                    //{
                    //bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                    bulkCopy.ColumnMappings.Add(caseNumber, columnMaping[caseNumber]);
                    bulkCopy.ColumnMappings.Add(jsonData, columnMaping[jsonData]);
                    bulkCopy.ColumnMappings.Add(created, columnMaping[created]);
                    bulkCopy.ColumnMappings.Add(fileModified, columnMaping[fileModified]); 
                    //}

                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.DestinationTableName = Parameters[tableName];
                    bulkCopy.WriteToServer(Schema);
                }

                foreach (DataRow dr in Schema.Select("caseNumber<>''"))
                {
                    dr[jsonData] = "";
                }
            }
            //------------------------------Workaround because IConduct bug - cannot set long value to schema column------------------------------
        }

        private void updateSchemaOnLastSuccessfullImportStatsDate()
        {
            //update the last sucess file download only in one row - to update SF once.
            //was error on download some file - need to redownload nextime
            DataRow firstDR = this.Schema.Select("caseNumber<>''").FirstOrDefault(); // finds all rows with caseNumber!=null and selects first or null if haven't found any
            if (firstDR != null)
            {
                if (kWObject.minFailureDownloadFolderChangeDate < kWObject.maxSuccessDownloadFolderChangeDate)
                {
                    firstDR[folderChangeDate] = kWObject.minFailureDownloadFolderChangeDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                    kWObject.saveUnFailureFileDate(kWObject.minFailureDownloadFolderChangeDate);
                }
                else // no error on proccess - update the lastImportSuccessDate on SF CS 
                {
                    firstDR[folderChangeDate] = kWObject.maxSuccessDownloadFolderChangeDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                    //no neccesary because the success was already logged when proccessed
                    kWObject.saveUnFailureFileDate(kWObject.maxSuccessDownloadFolderChangeDate);
                }
            }
            //no row was added because no stats file was found 
            else
            {
                //insert row with new date - kWObject.maxSuccessDownloadFolderChangeDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                DataRow row = this.Schema.NewRow();
                row[folderChangeDate] = kWObject.maxSuccessDownloadFolderChangeDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
                this.Schema.Rows.Add(row);

                logger.Log("KW Last Date :" + row[folderChangeDate]);
            }
        }

        public static void UpdateSchema(string pcaseNumber, string statContent, DataTable schema, DateTime folderChangeDate)
        {
            UpdateSchema(pcaseNumber, statContent, schema, "KW", folderChangeDate);
        }
        public static void UpdateSchema(string pcaseNumber, string statContent, DataTable schema, string statSrc ="KW", DateTime? fileModifiedDate = null)
        {
            string[] lines = statContent.Split(
                                            new[] { "\r\n", "\r", "\n" },
                                            StringSplitOptions.None
                                            );
            foreach (var line in lines)
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    DataRow row = schema.NewRow();
                    if (statSrc == "KW")
                    {
                        row[caseNumber] = pcaseNumber;
                    }
                  
                    row[jsonData] = line;//"54654654645654564";// "0007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363000743630007436300074363";//;
                    //row[folderChangeDate] = folderChangeDate;// DateTime.ParseExact(folderChangeDate, "yyyy-MM-dd", null).ToString() ;
                    row[created] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    if (fileModifiedDate.HasValue)
                        row[fileModified] = fileModifiedDate.Value.ToString("yyyy-MM-dd HH:mm:ss");
                    schema.Rows.Add(row);
                }

            }
        }
        

        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Scanning the schema row by row and execute by row status
        /// </summary>
        /// <param name="schema"></param>
        /// <returns> The Schema after the rows has been scaned</returns>

        public Dictionary<string, string> HandleExtension()
        {
                //Create Kiteworks token once - save in Iconduct paramenter in order to save it on SF - Custom Setting
                //Parameters[token] = createToken();
                //Console.WriteLine(Parameters[token]);


                //distinguish between case to create KW directory AND case to delete the KW directory
                foreach (DataRow row in this.Schema.Rows)
                {
                    try
                    {
                        row[tokenSchemaCol] = Parameters[token];//update row scheme, In fact the token parameter is enough

                        switch (row[folderId].ToString().Trim())
                        {
                            case "":
                                CreateFolder(row);
                                break;
                            default:
                                DeleteFolder(row);
                                break;
                        }

                    }
                    catch (Exception e)
                    {
                    logger.Log(e.Message + ' ' + e.ToString(), LogLevel.ERROR);
                    }
                }
           
            return Parameters;
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Scanning the schema row by row and execute by row status
        /// </summary>
        /// <param name="schema"></param>
        /// <returns> The Schema after the rows has been scaned</returns>

        public Dictionary<string, string> createTokenHandleExtension()
        {
            try
            {
                //Create Kiteworks token once - save in Iconduct paramenter in order to save it on SF - Custom Setting
                Parameters[token] = createToken();
                Console.WriteLine(Parameters[token]);

                foreach (DataRow row in this.Schema.Rows)
                {
                    row[tokenSchemaCol] = Parameters[token];//update row scheme, In fact the token parameter is enough
                }
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), LogLevel.ERROR);
            }
            return Parameters;
        }


        public DataRow DeleteFolder(DataRow row)
        {
            try
            {
                logger.eventLog($"Start DeleteFolder func: " + row.Table, LogLevel.DEBUG);

                CustomWebResponse cwr = kWObject.DeleteFolder(row[folderId].ToString());

                row[errorMessage] = cwr.HeaderDictionary.ContainsKey("ErrorMessage") ? cwr.HeaderDictionary["ErrorMessage"] : "";
                row[statusCodeName] = cwr.HeaderDictionary.ContainsKey("StatusCode") ? cwr.HeaderDictionary["StatusCode"] : ""; 
                row[statusDescription] = cwr.HeaderDictionary.ContainsKey("StatusDescription") ? cwr.HeaderDictionary["StatusDescription"] : "";

                //if deletion succeed or folder already was deleted
                if (row[statusCodeName].Equals("NoContent") || row[statusCodeName].Equals("Forbidden"))// 204, 403
                {
                    row[folderId] = "|NULL|";//to empty the value in SF by IConduct
                    row[folderLink] = "|NULL|";
                    row[upldFileLink] = "|NULL|";
                    //row[errorMessage] = "|NULL|";
                }
            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), LogLevel.ERROR);
            }
            return row;
        }
        private void CreateFolder(DataRow row)
        {
            try
            {
                String CaseNumber = row[caseNumber].ToString();
                String CurrentFolderID = Parameters[currentFolderID];//get parent currentFolderID from CS
                logger.Log("KW Folder creation started for Case Number:" + CaseNumber);
            
                //get parent folder name
                String parentFolderName = CaseNumber.Substring(0, CaseNumber.Length - 3) + "000" + "-" + CaseNumber.Substring(0, CaseNumber.Length - 3) + "999";
                //find if parent folder exist
                CustomWebResponse cwr1 = kWObject.FindFolder(parentFolderName);

                logger.Log("KW Folder after getting parent Folder for Case Number:" + CaseNumber);

                row[errorMessage] = cwr1.HeaderDictionary.ContainsKey("ErrorMessage") ? cwr1.HeaderDictionary["ErrorMessage"] : "";
                row[statusCodeName] = cwr1.HeaderDictionary.ContainsKey("StatusCode") ? cwr1.HeaderDictionary["StatusCode"] : "";

                // If call success
                if (row[statusCodeName].Equals("OK"))//200
                {
                    dynamic data = JObject.Parse(cwr1.Body);
                    try
                    {
                    // Get Case CurrentFolderID if it exist:
                        CurrentFolderID = data["data"][0]["id"];
                    }
                    catch (Exception)
                    {
                        //create parent folder if it not already exists
                        CurrentFolderID = MakeCallOutCreateCurrentFolder(CaseNumber, row);
                    }
                            
                }
                //Error geting parent folder 
                else
                {
                    logger.Log("Error geting parent folder :" + parentFolderName+ " for case: "+ caseNumber+ ". " + row[errorMessage]);

                    row[errorMessage] = cwr1.HeaderDictionary.ContainsKey("ErrorMessage") ? cwr1.HeaderDictionary["ErrorMessage"] : "";
                    row[statusCodeName] = cwr1.HeaderDictionary.ContainsKey("StatusCode") ? cwr1.HeaderDictionary["StatusCode"] : "";

                    return;
                    //CurrentFolderID = MakeCallOutCreateCurrentFolder(CaseNumber, row);
                }
                row[currentFolderIDSchemaCol] = CurrentFolderID;//update parent currentFolderID to CS

                //Create KW Folder
                CustomWebResponse cwr = kWObject.CreateFolder(CurrentFolderID, CaseNumber, "Case Number " + CaseNumber);

                row[errorMessage] = cwr.HeaderDictionary.ContainsKey("ErrorMessage") ? cwr.HeaderDictionary["ErrorMessage"]: "";
                row[statusCodeName] = cwr.HeaderDictionary.ContainsKey("StatusCode") ? cwr.HeaderDictionary["StatusCode"] : "";  
                row[statusDescription] = cwr.HeaderDictionary["StatusCode"] + cwr.HeaderDictionary["StatusDescription"];

                // Debug status
                logger.Log("KW Folder creation for Case Number " + CaseNumber + " - folder creation in KW response: " + cwr.HeaderDictionary["StatusCode"]);

                String CaseFolderId="";
                String permalink="";
                // If success
                if (row[statusCodeName].Equals( "Created")|| row[statusCodeName].Equals("Conflict"))//201 , 409
                {
                    //If Folder Already Exist, get its Id
                    if (row[statusCodeName].Equals("Conflict") && cwr.HeaderDictionary.ContainsKey("Headers"))
                    {
                        List<String> headerLines = cwr.HeaderDictionary["Headers"].Split(
                                            new[] { "\r\n", "\r", "\n" },
                                            StringSplitOptions.None
                                            ).Where(s => s.Contains("X-Accellion-Location")).Select(s => s).ToList<String>();
                        if (headerLines.Count > 0)
                        {
                            //X-Accellion-Location: https://kw-upload1.skyboxsecurity.com/rest/folders/7119
                            CaseFolderId = headerLines[0].Substring(headerLines[0].IndexOf(Parameters[kwHostUrl]+ "/rest/folders/" )+ (Parameters[kwHostUrl] + "/rest/folders/").Length);
                            //permalink = Parameters[kwHostUrl] + "/#/folder/" + CaseFolderId; //Id not secured
                            //get folder Link
                            dynamic data = kWObject.FindFolderById(CaseFolderId);
                            if (data != null)
                            {
                                // Get Case Folder link:
                                permalink = data.permalink;
                            }
                        }
                    }
                    else
                    {
                        dynamic data = JObject.Parse(cwr.Body);
                        // Get Case Folder ID:
                        CaseFolderId = data.id;
                        permalink = data.permalink;
                    }

                    // Get File Request Link:
                    String RequestLink = kWObject.GetKWFileRequestLink(CaseFolderId);
                    if (RequestLink != "-1")
                    {
                        row[folderId] = CaseFolderId;
                        row[folderLink] = permalink;
                        row[upldFileLink] = RequestLink;
                        row[errorMessage] = "|NULL|";
                    }
                    else
                    {
                        row[errorMessage] += " KW Issue in RequestLink: " + CaseFolderId;
                        logger.Log("KW Issue in RequestLink: " + CaseFolderId);
                    }
                    // create File Request:
                    /*
                    string CreateKWFExitCode = CreateKWFileRequest(CaseFolderId);
                    if (CreateKWFExitCode == "OK")//200
                    {
                        // Get File Request ID:
                        String RequestFileID = GetKWFileRequestID(CaseFolderId);
                        if (RequestFileID != "-1")
                        {
                            // Get File Request Link:
                            String RequestLink = GetKWFileRequestLink(RequestFileID);
                            if (RequestLink != "-1")
                            {
                                row[folderId] = CaseFolderId;
                                row[folderLink] = permalink;
                                row[upldFileLink] = RequestLink;
                                row[errorMessage] = "|NULL|";
                            }
                            else
                            {
                                row[errorMessage] += " KW Issue in RequestLink: " + CaseFolderId;
                                logger.Log("KW Issue in RequestLink: " + CaseFolderId);
                            }
                        }
                        else

                        {
                            row[errorMessage] += " KW Issue in RequestFileID: " + CaseFolderId;
                            logger.Log("KW Issue in RequestFileID: " + CaseFolderId);
                        }
                    }
                    else
                    {
                        row[errorMessage] += " KW Issue in CreateKWFileRequest: " + CaseFolderId;
                        logger.Log("KW Issue in CreateKWFileRequest: " + CaseFolderId);
                    }
                    */
                }
                else
                {
                    row[errorMessage] += " KW Issue in New Case - Folder Creation for case: " + CaseNumber;
                    logger.Log("KW Issue in New Case - Folder Creation for case: " + CaseNumber);
                }


            }
            catch (Exception e)
            {
                logger.Log(e.ToString(), LogLevel.ERROR);
            }

        }
        private String CreateKWFileRequest(String FolderID)
        {

            logger.Log("KW New Case - CreateKWFileRequest started for Folder ID :" + FolderID);

            CustomWebResponse cwr = kWObject.FileRequest(FolderID);

            // @ status
            logger.Log("KW CreateKWFileRequest For " + FolderID + " Exit code: " + cwr.HeaderDictionary["StatusCode"]);
            logger.Log("KW New Case - CreateKWFileRequest finished for Folder ID :" + FolderID);
            return cwr.HeaderDictionary["StatusCode"];
        }

        // Get request ID from activities:
        private String GetKWFileRequestID(String FolderID)
        {
            logger.Log("KW New Case - GetKWFileRequestID started for Folder ID :" + FolderID);
            String RequestFileID = "-1";

            CustomWebResponse cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/folders/{FolderID}/activities?noDayBack=1&filter=all&type=mail&orderBy=created%3Adesc&mode=full",
                                        "GET",
                                        "application/json",
                                        "",
                                        getHeaders(),
                                        new Dictionary<string, string>() { { "X-Accellion-Version", "4.1" } });

            // If success
            if (cwr.HeaderDictionary["StatusCode"].Equals("OK"))//200
            {
                dynamic data = JObject.Parse(cwr.Body);
                // Get Case File ID:
                RequestFileID = data["data"][0]["data"]["request_file_ref"];

                //JObject data2 = (JObject)JsonConvert.DeserializeObject(cwr.Body);
                //string timeZone = data2["request_file_ref"].Value<string>();
                //RequestFileID = from p in data2["data"]["data"]
                //                select (string)p["request_file_ref"].FirstOrDefault<String>();
                            
            }
            logger.Log("KW New Case - GetKWFileRequestID finished for Folder ID :" + FolderID);
            return RequestFileID;
        }

        private String GetKWFileRequestLink(String RequestFileID)
        {
            logger.Log("KW New Case - GetKWFileRequestLink started for RequestFileID :" + RequestFileID);
            String RequestLink = "-1";

            CustomWebResponse cwr = CustomWebRequest.Execute(Parameters[kwHostUrl] + $"/rest/requestFile/{RequestFileID}",
                                        "GET",
                                        "application/json",
                                        "",
                                        getHeaders(),
                                        new Dictionary<string, string>() { { "X-Accellion-Version", "4.1" } });

            // If success
            if (cwr.HeaderDictionary["StatusCode"].Equals("OK"))//200
            {
                dynamic data = JObject.Parse(cwr.Body);
                // Get Case File Upload Link:
                RequestLink = data.uploadLink;

            }
            logger.Log("KW New Case - GetKWFileRequestLink Finished for RequestFileID :" + RequestFileID);
            return RequestLink;
        }

        private String MakeCallOutCreateCurrentFolder(String CaseNumber, DataRow row)
        {
            String parentFolderName = CaseNumber.Substring(0, CaseNumber.Length - 3) + "000" + "-" + CaseNumber.Substring(0, CaseNumber.Length - 3) + "999";
            logger.Log("KW New Case - MakeCallOutCreateCurrentFolder started for Case Number :" + CaseNumber);
            
            //String reqBody = "{\"name\":\"" + parentFolderName + "\",\"description\":\"Parent Folder for cases Numbers " + CaseNumber + " To \",\"syncable\":false,\"expire\":\"2116-07-17\",\"fileLifetime\": 0,\"secure\":false}";
            
            CustomWebResponse cwr = kWObject.CreateFolder(Parameters[rootFolderID], parentFolderName, "Parent Folder for case Numbers " + parentFolderName);

            // If success
            if (cwr.HeaderDictionary["StatusCode"].Equals("Created"))//201
            {
                dynamic data = JObject.Parse(cwr.Body);
                // Get Case File ID:
                String NewCurrentFolderId = data.id;

                logger.Log("KW New Case - MakeCallOutCreateCurrentFolder finished for Case Number :" + CaseNumber);
                return NewCurrentFolderId;

            }
            else
            {
                logger.Log("KW Issue in Create Parent folder " + CaseNumber);
                row[errorMessage] = cwr.HeaderDictionary.ContainsKey("ErrorMessage") ? cwr.HeaderDictionary["ErrorMessage"] : "";
                //KW_SendEmail.SendEmail("KW Issue in Create Parent folder", "Case Number: " + CaseNumber);
                return Parameters[currentFolderID];
            }
            
        }






        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to Create Kiteworks Token, Result will be insert to IConduct Interface Parameter
        /// </summary>
        /// <param name="row">each row of the schema</param>

        private string createToken()
        {
            //token is calculated by https://www.accellion.com/sites/default/files/resources/kiteworks-enterprise-api-overview-guide.pdf
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            Int32 nonce = Utils.GetRandomNumber(1, 1000000);
            string baseString = $"{Parameters[clientAppId]}|@@|{Parameters[userId]}|@@|{unixTimestamp}|@@|{nonce}";
            string signature = Utils.EncodeHMAC_SHA1(baseString, Parameters[sigKey]);
            string authCode = $"{Utils.Base64Encode(Parameters[clientAppId])}|@@|{Utils.Base64Encode(Parameters[userId])}|@@|{unixTimestamp}|@@|{nonce}|@@|{signature}";
            string requestBody = $"client_id={Parameters[clientAppId]}&client_secret={Parameters[clientAppSecretKey]}&grant_type=authorization_code&code={authCode}&redirect_uri={Parameters[redirectURI]}";

            String response = CustomWebRequest.Execute(Parameters[createTokenUrl],
                                "POST",
                                Parameters[contentType],
                                requestBody,
                                new Dictionary<HttpRequestHeader, string>()).Body;
            logger.Log("createToken response: " + response);
            dynamic data = JObject.Parse(response);

            //log error if access_token not in response
            return data.access_token;
        }

        private Dictionary<HttpRequestHeader, string> getHeaders()
        {
            return new Dictionary<HttpRequestHeader, string>() { { HttpRequestHeader.Authorization, $"Bearer {Parameters[token]}" } };
        }

      

    }
}
