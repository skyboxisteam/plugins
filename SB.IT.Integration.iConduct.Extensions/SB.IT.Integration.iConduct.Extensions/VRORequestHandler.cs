﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using Newtonsoft.Json;
using SB.IT.Integration.iConduct.Extensions.VROTypes;
using System.Net;
using Newtonsoft.Json.Linq;

namespace SB.IT.Integration.iConduct.Extensions
{
  
    public class VRORequestHandler
    {
        //params

        const string cloneURL = "cloneURL";
        const string deleteURL = "deleteURL";
        const string basic = "basic";
        const string startStopURL = "startStopURL";


        //schema

        const string status = "status";
        const string locationClone = "locationClone";
        const string locationDelete = "locationDelete";
        const string locationStart = "locationStart";
        const string locationStop = "locationStop";
        const string newCloneStatus = "newCloneStatus";
        const string newDeleteStatus = "newDeleteStatus";
        const string newStartStatus = "newStartStatus";
        const string newStopStatus = "newStopStatus";
        const string newName = "newName";
        const string originalName = "originalName";
        const string ip = "IP";
        const string errorMessage = "errorMessage";

        //Internal

        public Dictionary<string, string> Parameters;
        public DataTable Schema;
        const string error = "Error";
        const string running = "running";

        const string sSource = "iConduct Extension Plugin";
        const string sLog = "Application";
        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        ///  Constractor
        /// </summary>
        /// <param name="parameters">The parameters from Iconduct</param>
        /// <param name="schema"> The schema from Iconduct</param>

        public VRORequestHandler(Dictionary<string, string> parameters, DataTable schema)
        {
            this.Parameters = parameters;
            this.Schema = schema;
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Scanning the schema row by row and execute by row status
        /// </summary>
        /// <param name="schema"></param>
        /// <returns> The Schema after the rows has been scaned</returns>

        public DataTable HandleExtensionCall(DataTable schema)
        {
            try
            {

                foreach (DataRow row in schema.Rows)
                {

                    switch (row[status].ToString())
                    {
                        case "clone":
                            CloneRequest(row);
                            break;

                        case "delete":
                            DeleteRequest(row);
                            break;

                        case "start":
                            StartRequest(row);
                            break;

                        case "stop":
                            StopeRequest(row);
                            break;

                        case "cloneStatus":
                            CloneStatusRequest(row);
                            break;

                        case "deleteStatus":
                            DeleteStatusRequest(row);
                            break;

                        case "startStatus":
                            StartStatusRequest(row);
                            break;

                        case "stopStatus":
                            StopStatusRequest(row);
                            break;


                    }

                }
            }
            catch (Exception e)
            {
                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);
                EventLog.WriteEntry(sSource, $"Error in {System.Reflection.MethodBase.GetCurrentMethod().Name} - {e.ToString()}", EventLogEntryType.Error);
            }
            return schema;
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Handle request to clone a VM. result will be the url of the API call to check the status of the cloning process
        /// </summary>
        /// <param name="row">each row of the schema</param>

        public void CloneRequest(DataRow row)
        {
                row[locationClone] = CustomWebRequest.Execute(Parameters[cloneURL], 
                                     "POST",
                                     JsonConvert.SerializeObject(new VRORequestBody(row[originalName].ToString(), row[newName].ToString())),
                                     getHeaders()).HeaderDictionary["Location"];
                row[newCloneStatus] = row[locationClone] != null ? running : error;
            Console.WriteLine(row[locationClone]);
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to start a VM. result will be the url of the API call to check the status of the start process
        /// </summary>
        /// <param name="row">each row of the schema</param>

        public void StartRequest(DataRow row)
        {
            row[locationStart] = CustomWebRequest.Execute(Parameters[startStopURL],
                                 "POST",
                                 JsonConvert.SerializeObject(new VRORequestBody(row[newName].ToString(),1, "boolean")),
                                 getHeaders()).HeaderDictionary["Location"];
            Console.WriteLine(row[locationStart]);
            row[newStartStatus] = row[locationStart] != null ? running : error;
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to stop a VM. result will be the url of the API call to check the status of the stop process
        /// </summary>
        /// <param name="row">each row of the schema</param>

        public void StopeRequest(DataRow row)
        {
            row[locationStop] = CustomWebRequest.Execute(Parameters[startStopURL],
                                 "POST",
                                 JsonConvert.SerializeObject(new VRORequestBody(row[newName].ToString(), 0, "boolean")),
                                 getHeaders()).HeaderDictionary["Location"];
            row[newStopStatus] = row[locationStop] != null ? running : error;
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to delete a VM. result will be the url of the API call to check the status of the deleting process
        /// </summary>
        /// <param name="row">each row of the schema</param>
        public void DeleteRequest(DataRow row)
        {
            row[locationDelete] = CustomWebRequest.Execute(Parameters[deleteURL],
                                   "POST",
                                   JsonConvert.SerializeObject(new VRORequestBody(row[newName].ToString())),
                                   getHeaders()).HeaderDictionary["Location"];
            row[newDeleteStatus] = row[locationDelete] != null ? running : error;
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to check the clone request status. 
        /// </summary>
        /// <param name="row"></param>

        public void CloneStatusRequest(DataRow row)
        {
            checkStatus(row[status].ToString(), row, JsonConvert.DeserializeObject<VROStatusResponse>(
                                           CustomWebRequest.Execute(row[locationClone].ToString(), "GET", null, getHeaders()
                                           ).Body));
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to check the clone request status. 
        /// </summary>
        /// <param name="row"></param>

        public void StartStatusRequest(DataRow row)
        {

            checkStatus(row[status].ToString(), row, JsonConvert.DeserializeObject<VROStatusResponse>(
                                           CustomWebRequest.Execute(row[locationStart].ToString(), "GET", null, getHeaders()
                                           ).Body));
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to check the clone request status. 
        /// </summary>
        /// <param name="row"></param>

        public void StopStatusRequest(DataRow row)
        {

            checkStatus(row[status].ToString(), row, JsonConvert.DeserializeObject<VROStatusResponse>(
                                           CustomWebRequest.Execute(row[locationStop].ToString(), "GET", null, getHeaders()
                                           ).Body));
        }


        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Handle request to check the delete request status. 
        /// </summary>
        /// <param name="row"></param>


        public void DeleteStatusRequest(DataRow row)
        {
            checkStatus(row[status].ToString(), row,JsonConvert.DeserializeObject<VROStatusResponse>(
                                            CustomWebRequest.Execute(row[locationDelete].ToString(), "GET", null, getHeaders()
                                            ).Body));      
        }

        //------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Help method that will analize the response of check status call and will update the row accordingly 
        /// </summary>
        /// <param name="status"> The VM Status</param>
        /// <param name="row">The row that is being itereate from the Schema</param>
        /// <param name="responseBody">Responce Json Body</param>
      
        private void checkStatus(string status, DataRow row, VROStatusResponse responseBody)
        {
            try
            {
                  switch (status)
                  {

                      case "cloneStatus":
                          row[newCloneStatus] = responseBody.State;
                          break;

                      case "deleteStatus":
                          row[newDeleteStatus] = responseBody.State;
                          break;

                      case "startStatus":
                          row[newStartStatus] = responseBody.State;
                          break;

                      case "stopStatus":
                          row[newStopStatus] = responseBody.State;
                          break;


                  }
                if (status == "cloneStatus" && responseBody.State == "completed")
                {
                     JObject results = JObject.Parse(responseBody.OutputParameters[0].value.ToString());
                     row[ip] = results["string"]["value"];
                }
                if (responseBody.State == "failed")                 
                    row[errorMessage] = responseBody.ContentException;

                }
            catch (Exception e)
            {
                row[errorMessage] = e.Message;
            }

        }

        private Dictionary<HttpRequestHeader, string> getHeaders()
        {
            return new Dictionary<HttpRequestHeader, string>() { { HttpRequestHeader.Authorization, Parameters[basic] } };
        }

    }
}
