﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SB.IT.Integration.iConduct.Extensions.Logger;

namespace SB.IT.Integration.iConduct.Extensions
{
    public class DictionaryLog : SBLogsHandler
    {
        //Dictionary Log schema
        const string current_dic_ver = "current_dic_ver";
        const string production = "production";
        const string version = "version";
        const string dic_path = "dic_path";
        const string dic_downloaded = "dic_downloaded";

        public DictionaryLog(Dictionary<string, string> parameters, DataTable schema):base(parameters, schema)
        {
            mapColumnToIndex = new Dictionary<int, string>() { { 0, date }, { 1, production }, { 2, version }, { 3, customer }, { 4, current_dic_ver }, { 5, ip }, { 6, dic_path }, { 7, dic_downloaded } };
        }
        /// <summary>
        /// Overrides the base DownloadFile because need to decryptFile
        /// </summary>
        protected override void DownloadFile(string filePath)
        {
            Files fileUtil = new Files();
            string targetPath = fileUtil.DownloadFile(filePath, "DictionaryLog.txt");

            // If DownloadFile success
            if (File.Exists(targetPath))
            {
                //int daysAgo = 2;
                fileUtil.RemoveLinesByDate(targetPath, int.Parse(this.Parameters[daysAgo]));//Delete from file all the lines older then X last days
                string decryptFile = this.decryptFile(targetPath, this.Parameters[decryptBatchFile]);
                
                string line;
                // Read the file and display it line by line.  
                StreamReader file = new StreamReader(decryptFile);
                while ((line = file.ReadLine()) != null)
                {
                    AddRowToSchema(line);
                }

                file.Close();
                //delete all proccessed files
                try
                {
                    File.Delete(targetPath);
                }
                catch (Exception ex)
                {
                    logger.Log("Error on delete: " + targetPath + ". " + ex.Message, LogLevel.WARNING);
                }
                try
                {
                    File.Delete(decryptFile);
                }
                catch (Exception ex)
                {
                    logger.Log("Error on delete: " + decryptFile + ". " + ex.Message, LogLevel.WARNING);
                }
            }
        }
    }
}
