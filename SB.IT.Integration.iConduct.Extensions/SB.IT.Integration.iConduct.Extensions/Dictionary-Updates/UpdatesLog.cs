﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Extensions
{
    public class UpdatesLog : SBLogsHandler
    {
        //Updates Log schema
        const string product = "product";
        const string current_version = "current_version";
        const string available_update = "available_update";
        const string action = "action";

        public UpdatesLog(Dictionary<string, string> parameters, DataTable schema):base(parameters, schema)
        {
            mapColumnToIndex = new Dictionary<int, string>() { { 0, date }, { 1, customer }, { 2, ip }, { 3, product }, { 4, current_version }, { 5, available_update }, { 6, action } };
        }
    }
}
