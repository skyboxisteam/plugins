﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Extensions
{
    class KWObject
    {
        public KWObject()
        {
            minFailureDownloadFolderChangeDate = DateTime.MaxValue;
        }
        public DateTime maxSuccessDownloadFolderChangeDate { get; set; }
        public DateTime minFailureDownloadFolderChangeDate { get; set; }

        public const String fileSeletedMessage = "{\n    \"message\": \"Entity is deleted\"\n}"; 
    }
}
