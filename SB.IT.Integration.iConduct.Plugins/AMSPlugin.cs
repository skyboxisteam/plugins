﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IConduct.SDK;
using SB.IT.Integration.iConduct.Extensions;


namespace SB.IT.Integration.iConduct.Plugins
{
    public class AMSPlugin : IPlugin
    {
        public bool Execute(PluginParam param)
        {
            AMSHandler ams = new AMSHandler(param.Parameters, param.Schema);
            ams.HandleAMSImport();
            return true;
        }
    }
}
