﻿using IConduct.SDK;
using SB.IT.Integration.iConduct.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Plugins
{
    class TestPlugin : IPlugin
    {
        public bool Execute(PluginParam param)
        {
            TestHandler test = new TestHandler(param.Parameters, param.Schema);
            test.HandleExtensionCreate();
            return true;
        }
    }
}
