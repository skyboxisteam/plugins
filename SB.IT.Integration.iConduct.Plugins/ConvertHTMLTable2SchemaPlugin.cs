﻿using IConduct.SDK;
using SB.IT.Integration.iConduct.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.IT.Integration.iConduct.Plugins
{
    class ConvertHTMLTable2SchemaPlugin : IPlugin
    {
        public bool Execute(PluginParam param)
        {
            ConvertHTMLTable2SchemaHandler convertHTMLTable2Schema = new ConvertHTMLTable2SchemaHandler(param.Parameters, param.Schema);
            param.Schema = convertHTMLTable2Schema.HandleExtensionCall();
            return true;
        }
    }
}
