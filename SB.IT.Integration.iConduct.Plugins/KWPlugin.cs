﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IConduct.SDK;
using SB.IT.Integration.iConduct.Extensions;


namespace SB.IT.Integration.iConduct.Plugins
{
    public class KWPlugin : IPlugin
    {
        public bool Execute(PluginParam param)
        {
            KWRequestHandler KW = new KWRequestHandler(param.Parameters, param.Schema);
            KW.HandleExtension();
            return true;
        }
    }
}
