﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IConduct.SDK;
using SB.IT.Integration.iConduct.Extensions;


namespace SB.IT.Integration.iConduct.Plugins
{
    public class VROWebRequestPlugin : IPlugin
    {
        public bool Execute(PluginParam param)
        {
            VRORequestHandler vms = new VRORequestHandler(param.Parameters, param.Schema);
            param.Schema = vms.HandleExtensionCall(param.Schema);
            return true;
        }
    }
}
