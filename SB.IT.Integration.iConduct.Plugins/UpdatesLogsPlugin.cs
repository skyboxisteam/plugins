﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IConduct.SDK;
using SB.IT.Integration.iConduct.Extensions;


namespace SB.IT.Integration.iConduct.Plugins
{
    public class UpdatesLogsPlugin : IPlugin
    {
        public bool Execute(PluginParam param)
        {
            SBLogsHandler log = new UpdatesLog(param.Parameters, param.Schema);
            log.HandleExtension();
         
            return true;
        }
    }
}
