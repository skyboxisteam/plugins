﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SB.IT.Integration.iConduct.Extensions;
using System.Data;

namespace SB.IT.Integration.ConsoleTests
{
    class Program
    {

        static string createTokenUrl = "https://kw-upload1.skyboxsecurity.com/oauth/token";
        static string deletFolderUrl = "https://kw-upload1.skyboxsecurity.com/rest/folders/";
        static string contentType = "application/x-www-form-urlencoded";
        static string ClientAppId = "2b9651f0-bbf0-5977-beb2-1c1745cf4857";
        static string UserId = "KW_SF_APIUser@skyboxsecurity.com";
        static string SigKey = "5VaVdsvyhrX5L0hZzcA1GnxW";
        static string RedirectURI = "https%3A%2F%2Fkw-upload1.skyboxsecurity.com%2Foauth_callback.php";
        static string ClientAppSecretKey = "jesItmo1";
        static string kwHostUrl = "https://kw-upload1.skyboxsecurity.com";
        static string token = "469ebc1e884967d19d00f4d84b2a17817ebf9f68";
        static string rootFolderID = "16";
        static string currentFolderID = "53424";
        static string lastStatsImport = "2019-03-28T00:52:39.000Z";
        static string apiVersion = "9.1";
        static string folderExpired = "2116-07-17";
        static string maxUploadFileCount = "20.0";
        static string fileName = ".zip";
        static string connectionString = "server =skybox-db-srv; database = DevDB; Integrated Security = SSPI";
        static string tableName = "Stats_Buffer";
        static string configTableName = "Config";
        static string IconductBugWorkaround = "true";
        static string htmlConvert = "<html><head><meta http-equiv=\"content - type\" content=\"text / html; charset = utf - 8\"></head><body><table> <tr> <td>AccountNumber</td> <td>Document Number</td> <td>Amount</td> <td>Department</td> <td>Accounting Period: Start Date</td> <td>Subsidiary: Name</td> <td>Transaction ID</td> <td>Name (GL-style)</td> <td>Account Type</td> </tr> <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>1000 - Checking</td> <td>Bank</td> </tr> <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>11000 - CASH</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1269</td> <td>=-1616</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>10669</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1104</td> <td>=1616</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>19457</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-33054.96</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>2007</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1393</td> <td>=33054.96</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>20867</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1103</td> <td>=26929</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>19452</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1310</td> <td>=-26929</td> <td>Balance Sheet</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>14992</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1000</td> <td>=527657.67</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>559</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1135</td> <td>=30000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5188</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>2</td> <td>=-24000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1499</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1034</td> <td>=0</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2332</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1072</td> <td>=-391.4</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2374</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1073</td> <td>=-35.68</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2375</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-33278.62</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1995</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-9976.48</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1998</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1074</td> <td>=-4.01</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2376</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1075</td> <td>=0.02</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2377</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3190</td> <td>=-764.17</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5305</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>1</td> <td>=-6616.19</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1399</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-11657.87</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1997</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1040</td> <td>=45000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2373</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1025</td> <td>=46016.66</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2037</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1026</td> <td>=9000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2038</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-15678.61</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1999</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-1555.07</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2009</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-6500.07</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2011</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1024</td> <td>=126724.99</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2036</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1035</td> <td>=-18</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2333</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1134</td> <td>=800</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5187</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-15778.54</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2008</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3</td> <td>=-6000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1599</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>4</td> <td>=-21920.46</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1699</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1023</td> <td>=5670</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2035</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7359</td> <td>=-18459.25</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2044</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1021</td> <td>=13837</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2033</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1036</td> <td>=-52</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2334</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1022</td> <td>=15766.6</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2034</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1037</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2335</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1031</td> <td>=4698</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2215</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-15144.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1828</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1032</td> <td>=159331.48</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2216</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1038</td> <td>=-60</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2336</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3192</td> <td>=-2164.35</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1839</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1033</td> <td>=72976.9</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2217</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1020</td> <td>=29959</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2032</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-33054.96</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>20870</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-1477.89</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2005</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-34.51</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2003</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-390.17</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2004</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14994.39</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2006</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-2476.2</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2012</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1001</td> <td>=-307953.39</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1842</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1038</td> <td>=486150</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2233</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-3361.49</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2058</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-13422.67</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2001</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1016</td> <td>=24500</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2028</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-52171.43</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>4436</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>Wire</td> <td>=-1209.4</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2057</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6735.61</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2061</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-75807.94</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2002</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1017</td> <td>=39206</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2029</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1122</td> <td>=25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>4437</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3191</td> <td>=-3177.5</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2059</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1019</td> <td>=90745</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2031</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1039</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2337</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1018</td> <td>=52823</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2030</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3195</td> <td>=-1492</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1837</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1015</td> <td>=13020</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2027</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1014</td> <td>=6502</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2026</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-6.95</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2000</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1013</td> <td>=53849</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2025</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1012</td> <td>=43922.44</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2024</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1011</td> <td>=29895</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2023</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1037</td> <td>=20698</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2221</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1006</td> <td>=6000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2018</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-1143</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2056</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1040</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2338</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3196</td> <td>=-3491.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5306</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1010</td> <td>=11840</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2022</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3193</td> <td>=-209.2</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1829</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>TRUS1000</td> <td>=1000000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1835</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1035</td> <td>=39011.94</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2219</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1036</td> <td>=160166</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2220</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1041</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2339</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1340</td> <td>=3380.88</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>16764</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7406</td> <td>=-3380.88</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1821</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1027</td> <td>=2250</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2039</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>Wire</td> <td>=-4137.3</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1833</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-55.43</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1834</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEIL1298</td> <td>=-1000000</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>16966</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-12265.66</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1832</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1009</td> <td>=407635.62</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2021</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-33276.44</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2130</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-18911.53</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2133</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-13263.18</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2137</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-10000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1831</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-15378.11</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2134</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-7780.31</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2145</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7410</td> <td>=-40852.18</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2128</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6121.7</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2147</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-29556.15</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2131</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-5947.65</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2149</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-36338.07</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2129</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-2421.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2141</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8932.67</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2139</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-9483.7</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2138</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-761.75</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1826</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-13827.44</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2135</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7409</td> <td>=-54428.9</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2127</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-3967.76</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2151</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-15443.02</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2176</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1076</td> <td>=11.12</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2378</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1077</td> <td>=23.69</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2379</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1078</td> <td>=2.32</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2380</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6314.42</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2329</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-559.5</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2118</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6235.61</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2323</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>5</td> <td>=-1584.57</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1700</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1079</td> <td>=17.99</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2381</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-199.02</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2125</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1003</td> <td>=10010</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1993</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1007</td> <td>=6000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2019</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8475.01</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2119</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-12668.23</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2152</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8426</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2063</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-2800</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2065</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1008</td> <td>=514677.9</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2020</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1005</td> <td>=277300</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2017</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1034</td> <td>=44775</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2218</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14980.94</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2325</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1042</td> <td>=-17.5</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2340</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-12184.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2184</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1004</td> <td>=229189.99</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1994</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1002</td> <td>=8121.76</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1992</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14625.71</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2214</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8017.36</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2121</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-1708.56</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2327</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8748.92</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2311</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1039</td> <td>=58948.95</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2234</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1001</td> <td>=114225</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1991</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1000</td> <td>=60572.77</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1990</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3194</td> <td>=-19310.92</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2060</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1044</td> <td>=-30</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2342</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1043</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2341</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-1551.5</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>5202</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-11220.29</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2733</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1271</td> <td>=25889</td> <td>Balance Sheet</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>10671</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1030</td> <td>=43680</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2042</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1146</td> <td>=-25889</td> <td>Balance Sheet</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>5209</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-4265.2</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2317</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-2001.58</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2312</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-28051.11</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2758</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3198</td> <td>=-25889</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>10673</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-24224.98</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2722</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14548.12</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2751</td> <td>1";
        static string filePath = "https://s3-us-west-2.amazonaws.com/skybox-dictionary-logs/sb_request.log";//Dictionary log
        static string decryptBatchFile = "c:";
        static string daysAgo = "2";
        static string limitSearchRslt = "3";
        static string amsHostName = "skyc-01";
        static string amsUserName = "skyboxview";
        static string amsPassword = "skyboxview";
        static string amsLocalPath = @"C:\repository\AMS";
        static string amsRemotePath = "/tmp/rmsfiles/prod";
        static string amsFileName = "stats.log";


        //debug your code here
        static Dictionary<string, string> paramsIconduct = new Dictionary<string, string>()
            {
                { "createTokenUrl",createTokenUrl },
                { "deletFolderUrl",deletFolderUrl },
                { "contentType",contentType },
                { "ClientAppId",ClientAppId },
                { "UserId",UserId },
                { "SigKey",SigKey },
                { "RedirectURI",RedirectURI },
                { "ClientAppSecretKey",ClientAppSecretKey },
                { "kwHostUrl",kwHostUrl },
                { "token",token },
                { "rootFolderID",rootFolderID },
                { "currentFolderID",currentFolderID },
                { "lastStatsImport",lastStatsImport },
                { "apiVersion",apiVersion },
                { "folderExpired",folderExpired },
                { "maxUploadFileCount",maxUploadFileCount },
                { "fileName",fileName },
                { "tableName",tableName },
                { "configTableName",configTableName },
                { "connectionString",connectionString },
                { "IconductBugWorkaround",IconductBugWorkaround },
                { "html",htmlConvert },
                { "filePath",filePath },
                { "decryptBatchFile",decryptBatchFile },
                { "daysAgo",daysAgo },
                { "limitSearchRslt",limitSearchRslt },
                { "amsHostName",amsHostName },
                { "amsUserName",amsUserName },
                { "amsPassword",amsPassword },
                { "amsLocalPath",amsLocalPath },
                { "amsRemotePath",amsRemotePath },
                { "amsFileName",amsFileName },
                {"debug", "false" }

            };

        static private void resetParms()
        {
            paramsIconduct = new Dictionary<string, string>()
            {
                { "createTokenUrl",createTokenUrl },
                { "deletFolderUrl",deletFolderUrl },
                { "contentType",contentType },
                { "ClientAppId",ClientAppId },
                { "UserId",UserId },
                { "SigKey",SigKey },
                { "RedirectURI",RedirectURI },
                { "ClientAppSecretKey",ClientAppSecretKey },
                { "kwHostUrl",kwHostUrl },
                { "token",token },
                { "rootFolderID",rootFolderID },
                { "currentFolderID",currentFolderID },
                { "lastStatsImport",lastStatsImport },
                { "apiVersion",apiVersion },
                { "folderExpired",folderExpired },
                { "maxUploadFileCount",maxUploadFileCount },
                { "fileName",fileName },
                { "tableName",tableName },
                { "connectionString",connectionString },
                { "IconductBugWorkaround",IconductBugWorkaround },
                { "html",htmlConvert },
                { "filePath",filePath },
                { "decryptBatchFile",decryptBatchFile },
                { "daysAgo",daysAgo },
                {"debug", "false" }

            };
        }

        static void testKiteWorksImportFiles()
        {


            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            DataView view;

            //Test stat file  Import
            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "caseNumber";
            table.Columns.Add(column);

            // Create Second DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "jsonData";
            table.Columns.Add(column);

            // Create Third DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "folderChangeDate";
            table.Columns.Add(column);

            // Create forth DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ExecuteTime";
            table.Columns.Add(column);

            // Create forth DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "fileModified";
            table.Columns.Add(column);


            // Create a DataView using the DataTable.
            view = new DataView(table);
            KWRequestHandler kw = new KWRequestHandler(paramsIconduct, table);
            kw.ImportFilesPluginHandleExtension();
            //kw.DownloadFile("", "", "");
        }
        static void testAMSImport()
        {
            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            DataView view;

            //Test stat file  Import
            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "amsFolderId";
            table.Columns.Add(column);

            // Create Second DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "jsonData";
            table.Columns.Add(column);

            // Create Third DataColumn, set DataType, ColumnName and add to DataTable.       
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ExecuteTime";
            table.Columns.Add(column);

            // Create a DataView using the DataTable.
            view = new DataView(table);
            AMSHandler ams = new AMSHandler(paramsIconduct, table);
            ams.HandleAMSImport();
            //kw.DownloadFile("", "", "");
        }

        static void testKiteWorks()
       {
            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            DataView view;


            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Token";
            table.Columns.Add(column);

            // Create Second DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "KW_FolderID__c";
            table.Columns.Add(column);


            // Create Third DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "StatusCodeName";
            table.Columns.Add(column);

            // Create Forth DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "StatusDescription";
            table.Columns.Add(column);
            // Create Fourth-2 column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "ErrorMessage";
            table.Columns.Add(column);

            // Create 5th DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "KW_CaseFolderUploadLink__c";
            table.Columns.Add(column);
            // Create 6th DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "KW_Folder_Link2__c";
            table.Columns.Add(column);
            // Create 7th DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CaseNumber";
            table.Columns.Add(column);
            // Create 8th DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CurrentFolderID";
            table.Columns.Add(column);
            


            //Test DeleteFolder
            row = table.NewRow();
            row["KW_FolderID__c"] = "66596";
            //table.Rows.Add(row);

            row = table.NewRow();
            row["KW_FolderID__c"] = "25851";
            //table.Rows.Add(row);

            row = table.NewRow();
            row["KW_FolderID__c"] = "24568";
            row["CurrentFolderID"] = "53424";
            //table.Rows.Add(row);

            //Test CreateFolder
            row = table.NewRow();
            row["CaseNumber"] = "00080966";// "00090012";// "00012000";
            row["KW_FolderID__c"] = "";
            table.Rows.Add(row);

            // Create a DataView using the DataTable.
            view = new DataView(table);
            KWRequestHandler kw = new KWRequestHandler(paramsIconduct, table);

            //kw.Log("hello");
            //kw.createTokenHandleExtension();
            kw.HandleExtension();



        }

        static void testHTMLConvert()
        {
            string htmlConvert = "<html><head><meta http-equiv=\"content - type\" content=\"text / html; charset = utf - 8\"></head><body><table> <tr> <td>AccountNumber</td> <td>Document Number</td> <td>Amount</td> <td>Department</td> <td>Accounting Period: Start Date</td> <td>Subsidiary: Name</td> <td>Transaction ID</td> <td>Name (GL-style)</td> <td>Account Type</td> </tr> <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>1000 - Checking</td> <td>Bank</td> </tr> <tr> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td></td> <td>11000 - CASH</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1269</td> <td>=-1616</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>10669</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1104</td> <td>=1616</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>19457</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-33054.96</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>2007</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1393</td> <td>=33054.96</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>20867</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1103</td> <td>=26929</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>19452</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1310</td> <td>=-26929</td> <td>Balance Sheet</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>14992</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1000</td> <td>=527657.67</td> <td>- No Department -</td> <td>12/1/2016</td> <td>Skybox Inc</td> <td>559</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1135</td> <td>=30000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5188</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>2</td> <td>=-24000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1499</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1034</td> <td>=0</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2332</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1072</td> <td>=-391.4</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2374</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1073</td> <td>=-35.68</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2375</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-33278.62</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1995</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-9976.48</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1998</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1074</td> <td>=-4.01</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2376</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1075</td> <td>=0.02</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2377</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3190</td> <td>=-764.17</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5305</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>1</td> <td>=-6616.19</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1399</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-11657.87</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1997</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1040</td> <td>=45000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2373</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1025</td> <td>=46016.66</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2037</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1026</td> <td>=9000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2038</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-15678.61</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1999</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-1555.07</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2009</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-6500.07</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2011</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1024</td> <td>=126724.99</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2036</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1035</td> <td>=-18</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2333</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1134</td> <td>=800</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5187</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-15778.54</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2008</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3</td> <td>=-6000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1599</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>4</td> <td>=-21920.46</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1699</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1023</td> <td>=5670</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2035</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7359</td> <td>=-18459.25</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2044</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1021</td> <td>=13837</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2033</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1036</td> <td>=-52</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2334</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1022</td> <td>=15766.6</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2034</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1037</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2335</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1031</td> <td>=4698</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2215</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-15144.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1828</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1032</td> <td>=159331.48</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2216</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1038</td> <td>=-60</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2336</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3192</td> <td>=-2164.35</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1839</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1033</td> <td>=72976.9</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2217</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1020</td> <td>=29959</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2032</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-33054.96</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>20870</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-1477.89</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2005</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-34.51</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2003</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-390.17</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2004</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14994.39</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2006</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-2476.2</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2012</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1001</td> <td>=-307953.39</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1842</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1038</td> <td>=486150</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2233</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-3361.49</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2058</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-13422.67</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2001</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1016</td> <td>=24500</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2028</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-52171.43</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>4436</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>Wire</td> <td>=-1209.4</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2057</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6735.61</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2061</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-75807.94</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2002</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1017</td> <td>=39206</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2029</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1122</td> <td>=25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>4437</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3191</td> <td>=-3177.5</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2059</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1019</td> <td>=90745</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2031</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1039</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2337</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1018</td> <td>=52823</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2030</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3195</td> <td>=-1492</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1837</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1015</td> <td>=13020</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2027</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1014</td> <td>=6502</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2026</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-6.95</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2000</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1013</td> <td>=53849</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2025</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1012</td> <td>=43922.44</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2024</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1011</td> <td>=29895</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2023</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1037</td> <td>=20698</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2221</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1006</td> <td>=6000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2018</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-1143</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2056</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1040</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2338</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3196</td> <td>=-3491.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>5306</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1010</td> <td>=11840</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2022</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3193</td> <td>=-209.2</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1829</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>TRUS1000</td> <td>=1000000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1835</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1035</td> <td>=39011.94</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2219</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1036</td> <td>=160166</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2220</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1041</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2339</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1340</td> <td>=3380.88</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>16764</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7406</td> <td>=-3380.88</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1821</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1027</td> <td>=2250</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2039</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>Wire</td> <td>=-4137.3</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1833</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-55.43</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1834</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEIL1298</td> <td>=-1000000</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>16966</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-12265.66</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1832</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1009</td> <td>=407635.62</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2021</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-33276.44</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2130</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-18911.53</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2133</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-13263.18</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2137</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-10000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1831</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-15378.11</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2134</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-7780.31</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2145</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7410</td> <td>=-40852.18</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2128</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6121.7</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2147</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-29556.15</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2131</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-5947.65</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2149</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-36338.07</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2129</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-2421.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2141</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8932.67</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2139</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-9483.7</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2138</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-761.75</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1826</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-13827.44</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2135</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>7409</td> <td>=-54428.9</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2127</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-3967.76</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2151</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-15443.02</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2176</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1076</td> <td>=11.12</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2378</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1077</td> <td>=23.69</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2379</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1078</td> <td>=2.32</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2380</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6314.42</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2329</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-559.5</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2118</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-6235.61</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2323</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>5</td> <td>=-1584.57</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1700</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1079</td> <td>=17.99</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2381</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-199.02</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2125</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1003</td> <td>=10010</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1993</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1007</td> <td>=6000</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2019</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8475.01</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2119</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-12668.23</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2152</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8426</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2063</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-2800</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2065</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1008</td> <td>=514677.9</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2020</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1005</td> <td>=277300</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2017</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1034</td> <td>=44775</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2218</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14980.94</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2325</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1042</td> <td>=-17.5</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2340</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-12184.26</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2184</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1004</td> <td>=229189.99</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1994</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1002</td> <td>=8121.76</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1992</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14625.71</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2214</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8017.36</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2121</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-1708.56</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2327</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-8748.92</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2311</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1039</td> <td>=58948.95</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2234</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1001</td> <td>=114225</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1991</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1000</td> <td>=60572.77</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>1990</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3194</td> <td>=-19310.92</td> <td>- No Department -</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2060</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1044</td> <td>=-30</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2342</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1043</td> <td>=-25</td> <td>Balance Sheet</td> <td>1/1/2017</td> <td>Skybox Inc</td> <td>2341</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>ACH</td> <td>=-1551.5</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>5202</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-11220.29</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2733</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1271</td> <td>=25889</td> <td>Balance Sheet</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>10671</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>PAYUS1030</td> <td>=43680</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2042</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>JEUS1146</td> <td>=-25889</td> <td>Balance Sheet</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>5209</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-4265.2</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2317</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-2001.58</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2312</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-28051.11</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2758</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>3198</td> <td>=-25889</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>10673</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-24224.98</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2722</td> <td>11000 - CASH:11001 - Comerica Checking 189441008</td> <td>Bank</td> </tr> <tr> <td>11001</td> <td>WT</td> <td>=-14548.12</td> <td>- No Department -</td> <td>2/1/2017</td> <td>Skybox Inc</td> <td>2751</td> <td>1";


            //debug your code here
            Dictionary<string, string> paramsIconduct = new Dictionary<string, string>()
            {
                { "html",htmlConvert },
                {"debug", "false" }

            };


            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            DataView view;

            //Test stat file  Import
            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "response";
            table.Columns.Add(column);

            //Test CreateFolder
            row = table.NewRow();
            row["response"] = htmlConvert;
            table.Rows.Add(row);

            // Create a DataView using the DataTable.
            view = new DataView(table);
            ConvertHTMLTable2SchemaHandler html = new ConvertHTMLTable2SchemaHandler(paramsIconduct, table);
            html.HandleExtensionCall();
        }


        static void testTest()
        {


            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            DataView view;

            //Test stat file  Import
            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "caseNumber";
            table.Columns.Add(column);


            // Create a DataView using the DataTable.
            view = new DataView(table);
            TestHandler test = new TestHandler(paramsIconduct, table);
            test.HandleExtensionCreate();
            test.HandleExtensionDelete();
        }
        static void Main(string[] args)
        {
            testKiteWorksImportFiles();
            testKiteWorks();
            testAMSImport();
            testUpdatesLog();
            testDictionaryLog();
            //testTest();
            testHTMLConvert();

            string cloneURL = "https://prod-vro01.il.skyboxsecurity.com:8281/vco/api/workflows/bfdd23a6-4b60-4bb1-8cec-1d2ce83880e0/executions";
            string deleteURL = "test";
            string pendingCloneURL = "https://prod-vro01.il.skyboxsecurity.com:8281/vco/api/workflows/bfdd23a6-4b60-4bb1-8cec-1d2ce83880e0/executions/4028d21b626b991701633f62d1180311/";
            string pendingDeleteURL = "test";
            string cloneHeaderKey = "test";
            string cloneStatus = "test";
            string peningStatus = "Pending";
            string pendingCloneStatus = "running";
            string pendingDeleteStatus = "test";
            string deleteStatus = "test";
            string locationStart = "https://prod-vro01.il.skyboxsecurity.com:8281/vco/api/workflows/53a9b066-9645-4773-aa63-7cdd2c0a96a4/executions/4028d21b64b609520164d3098c67005f/";
            string startStopURL = "https://prod-vro01.il.skyboxsecurity.com:8281/vco/api/workflows/53a9b066-9645-4773-aa63-7cdd2c0a96a4/executions";
            string basic = "Basic c2t5Ym94XHZtc3NvOmxvY2FsUjAwdA==";
            //debug your code here
            Dictionary<string, string> paramsIconduct = new Dictionary<string, string>()
            {
                { "cloneURL",cloneURL },
                {"deleteURL",deleteURL },
                {"pendingCloneURL",pendingCloneURL },
                {"pendingDeleteURL",pendingDeleteURL },
                {"cloneHeaderKey",cloneHeaderKey },
                {"cloneStatus",cloneStatus },
                {"pendingStatus",peningStatus },
                {"pendingCloneStatus",pendingCloneStatus },
                {"pendingDeleteStatus",pendingDeleteStatus },
                {"startStopURL",startStopURL },
                {"locationStart",locationStart },
                {"deleteStatus",deleteStatus },
                {"basic",basic }

            };


            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            DataView view;

            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "status";
            table.Columns.Add(column);

            // Create second column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "originalName";
            table.Columns.Add(column);

            // Create Third column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "newName";
            table.Columns.Add(column);

            // Create Fourth column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "LocationDelete";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "locationStart";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "locationStop";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "IP";
            table.Columns.Add(column);

            // Create Fourth column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "ErrorMessage";
            table.Columns.Add(column);

            // Create Fourth column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "LocationClone";
            table.Columns.Add(column);

           

            // Create Fourth column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "newDeleteStatus";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "newStopStatus";
            table.Columns.Add(column);


            // Create Fourth column.
            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "newCloneStatus";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "newStartStatus";
            table.Columns.Add(column);

            // Create new DataRow objects and add to DataTable.    
            // for (int i = 0; i < 10; i++)
            //  {
            row = table.NewRow();

            row["status"] = "startStatus";
            row["originalName"] = "vm-6000-daniel";
            row["newName"] = "guy2";
            row["locationStart"] = locationStart;
            row["locationClone"] = "https://prod-vro01.il.skyboxsecurity.com:8281/vco/api/workflows/bfdd23a6-4b60-4bb1-8cec-1d2ce83880e0/executions/4028d21b64b609520164d5c948f100bf/";

            //row["item"] = "item " + i.ToString();
            table.Rows.Add(row);
            //  }

            // Create a DataView using the DataTable.
            view = new DataView(table);
            VRORequestHandler wr = new VRORequestHandler(paramsIconduct, table);
            wr.HandleExtensionCall(table);

            // Set a DataGrid control's DataSource to the DataView.
            // dataGrid1.DataSource = view;
        }

        private static void testUpdatesLog()
        {
            filePath = "https://s3-us-west-2.amazonaws.com/skybox-update-logs/online_log.txt";
            daysAgo = "500";
            resetParms();

            DataTable table = new DataTable();
            DataColumn column;
            DataRow row;
            DataView view;

            //Test stat file  Import
            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "date";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "customer";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ip";
            table.Columns.Add(column);
            
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "product";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "current_version";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "available_update";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "action";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "externalId";
            table.Columns.Add(column);
            //
            //row = table.NewRow();
            //row["response"] = htmlConvert;
            //table.Rows.Add(row);

            // Create a DataView using the DataTable.
            view = new DataView(table);
            SBLogsHandler dicHandler = new UpdatesLog(paramsIconduct, table);
            dicHandler.HandleExtension();
        }
        private static void testDictionaryLog()
        {
            DataTable table = new DataTable();
            DataColumn column;
            //DataRow row;
            DataView view;

            //Test stat file  Import
            // Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "date";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "customer";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ip";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "production";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "version";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "dic_path";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "dic_downloaded";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "current_dic_ver";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "externalId";
            table.Columns.Add(column);

            // Create a DataView using the DataTable.
            view = new DataView(table);
            SBLogsHandler dicHandler = new DictionaryLog(paramsIconduct, table);
            dicHandler.HandleExtension();
        }
    }
}

