﻿using IConduct.SDK;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace IConduct.Plugins.HtmlTable2Schema
{
    public class ConvertHTMLTable2Schema : IPlugin
    {
        public bool Execute(PluginParam param)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(param.Parameters["html"]);
            DataTable dt = param.Schema;

            int i = 0;
            List<string> cols = new List<string>();
            foreach (HtmlAgilityPack.HtmlNode tr in doc.DocumentNode.SelectNodes("//tr"))
            {
                int j = 0;
                DataRow dr = dt.NewRow();
                foreach (HtmlAgilityPack.HtmlNode td in tr.ChildNodes)
                {
                    if (i == 0)
                    {
                        cols.Add(td.InnerText.Replace(' ', '_').Replace("(", "_").Replace(")", "_").Replace(":", "_").Replace("-", "_"));
                    }
                    else
                    {
                        string columnName = cols[j];
                        string value = td.InnerText;
                        
                        if(dt.Columns.Contains(columnName))
                        dr[columnName] = value;
                    }
                    j++;
                }
                dt.Rows.Add(dr);
                i++;
            }

            return true;
        }
    }
}
